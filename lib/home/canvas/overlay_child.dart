import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/key_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OverlayCanvasChild extends StatelessWidget {
  final int? _index;
  final bool isSelected;
  final SelectableWidget selectableWidget;

  const OverlayCanvasChild({
    Key? key,
    required int? index,
    required this.selectableWidget,
  })  : _index = index,
        isSelected = index == null,
        super(key: key);

  Widget _selectableWidgetBuilder(final BuildContext context) {
    final width = context.watch<CanvasProvider>().sizeProvider.width;
    final height = context.watch<CanvasProvider>().sizeProvider.height;

    final widget = selectableWidget;
    if (widget is st.SelectableText) {
      return widget.text(
        canvasHeight: height,
      );
    } else if (widget is SelectableImage) {
      return widget.image(
        canvasWidth: width,
        canvasHeight: height,
      );
    } else {
      return const ColoredBox(color: Colors.black);
    }
  }

  int? get index => isSelected ? null : _index;

  Widget _gestureHandler(final BuildContext context) {
    final canvasParentKey = context.watch<KeyProvider>().canvasParentKey;
    return Transform.rotate(
      angle: selectableWidget.rd,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => context
            .read<CanvasProvider>()
            .selectableWidgetsProvider
            .selectableWidgetOnTapAt(index),
        onSecondaryTapUp: (_) => context
            .read<CanvasProvider>()
            .selectableWidgetsProvider
            .selectableWidgetOnSecendaryTapUpAt(index, _, canvasParentKey),
        onLongPressStart: (_) => context
            .read<CanvasProvider>()
            .selectableWidgetsProvider
            .selectableWidgetOnLongPressStartAt(index, _, canvasParentKey),
        onPanUpdate: selectableWidget.isLocked
            ? null
            : (_) {
                if (!isSelected) {
                  context
                      .read<CanvasProvider>()
                      .selectableWidgetsProvider
                      .selectableWidgetOnTapAt(_index);
                }
                context
                    .read<CanvasProvider>()
                    .selectableWidgetsProvider
                    .onPanUpdate(_);
              },
        child: IgnorePointer(
          child: Opacity(
            opacity: 0,
            child: _selectableWidgetBuilder(context),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Offset>(
      future: context
          .watch<KeyProvider>()
          .localSelectablePositionToGlobal(selectableWidget),
      builder: (context, snapshot) {
        final offset = snapshot.data;
        if (offset == null) return const SizedBox();
        return Positioned(
          top: offset.dy,
          left: offset.dx,
          child: _gestureHandler(context),
        );
      },
    );
  }
}
