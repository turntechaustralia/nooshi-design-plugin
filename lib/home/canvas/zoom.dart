import 'package:flayout/components/hover_opacity.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ZoomButtons extends StatelessWidget {
  const ZoomButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HoverOpacity(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox.fromSize(
            size: const Size.square(40),
            child: Material(
              shape: const CircleBorder(),
              clipBehavior: Clip.hardEdge,
              child: InkWell(
                onTap: context.watch<CanvasProvider>().sizeProvider.zoomOut,
                child: const Icon(Icons.zoom_out),
              ),
            ),
          ),
          const SizedBox(width: 8),
          SizedBox.fromSize(
            size: const Size.square(40),
            child: Material(
              shape: const CircleBorder(),
              clipBehavior: Clip.hardEdge,
              child: InkWell(
                onTap: context.watch<CanvasProvider>().sizeProvider.zoomIn,
                child: const Icon(Icons.zoom_in),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
