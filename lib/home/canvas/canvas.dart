import 'package:flayout/home/canvas/context_menu.dart';
import 'package:flayout/home/canvas/drag_target.dart';
import 'package:flayout/home/canvas/interactive_canvas_section.dart';
import 'package:flayout/home/canvas/overlay_child.dart';
import 'package:flayout/home/canvas/selected_widget_tool_overlay.dart';
import 'package:flayout/home/canvas/top_canvas_gesture_handler.dart';
import 'package:flayout/home/canvas/zoom.dart';
import 'package:flayout/home/main/view.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/key_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CanvasArea extends StatelessWidget {
  const CanvasArea({Key? key}) : super(key: key);

  List<Widget> _getChildrenOverlay(final BuildContext context) {
    final selectedWidget = context
        .watch<CanvasProvider>()
        .selectableWidgetsProvider
        .selectedWidget;
    return [
      ...List.generate(
        context
            .watch<CanvasProvider>()
            .selectableWidgetsProvider
            .selectableWidgetsCount,
        (index) {
          final widget = context
              .read<CanvasProvider>()
              .selectableWidgetsProvider
              .widgetAt(index);
          return OverlayCanvasChild(
            index: index,
            selectableWidget: widget,
          );
        },
      ),
      if (selectedWidget != null)
        OverlayCanvasChild(
          index: null,
          selectableWidget: selectedWidget,
        ),
    ];
  }

  ///Creates Context-Menu
  Widget _contextMenuBuilder(final BuildContext context) {
    final contextMenuGlobalLocation =
        context.watch<CanvasProvider>().contextMenuProvider.contextMenuLocation;

    final selectedWidget = context
        .watch<CanvasProvider>()
        .selectableWidgetsProvider
        .selectedWidget;
    final contextMenuVisibility = context
        .watch<CanvasProvider>()
        .contextMenuProvider
        .contextMenuVisibility;

    return Positioned(
      top: contextMenuGlobalLocation.dy,
      left: contextMenuGlobalLocation.dx,
      child: ContextMenu(
        selectedWidget: selectedWidget,
        contextMenuVisibility: contextMenuVisibility,
      ),
    );
  }

  Widget _canvasBuilder(final BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        context
            .read<CanvasProvider>()
            .sizeProvider
            .setInitialSize(constraints.biggest);

        return TopCanvasGestureHandler(
          canvas: Stack(
            key: context.watch<KeyProvider>().canvasParentKey,
            children: [
              const Positioned.fill(
                child: InteractiveCanvasSection(),
              ),
              ..._getChildrenOverlay(context),
              ...SelectedWidgetToolOverlay(this).builder(context),
              _contextMenuBuilder(context),
              const Positioned.fill(
                child: CanvasDragTarget(),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _zoomBuilder(final BuildContext context) {
    final isBigLayoutAndIsToolboxOpen =
        context.watch<ToolboxProvider>().isToolboxOpen &&
            context.watch<CanvasProvider>().sizeProvider.isBigLayout;

    final right = isBigLayoutAndIsToolboxOpen
        ? EditorView.toolbarPlusDividerWidth + 10
        : 10.0;

    return AnimatedPositioned(
      bottom: 10,
      right: right,
      curve: Curves.easeInOut,
      duration: context.read<ToolboxProvider>().animationDuration,
      child: const ZoomButtons(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Stack(
        fit: StackFit.expand,
        children: [
          _canvasBuilder(context),
          _zoomBuilder(context),
        ],
      )
    );
  }
}
