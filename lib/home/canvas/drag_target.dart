import 'dart:async';

import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/key_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CanvasDragTarget extends StatelessWidget {
  const CanvasDragTarget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = context.watch<CanvasProvider>().sizeProvider.width;
    final height = context.watch<CanvasProvider>().sizeProvider.height;
    final scale = context.watch<CanvasProvider>().sizeProvider.scale;

    return DragTarget(
      onAcceptWithDetails: (_) async {
        final data = _.data;
        if (data is! SelectableWidget) return;

        final ctx = context.read<KeyProvider>().canvasKey.currentContext;
        if (ctx == null) return;

        final renderBox = ctx.findRenderObject() as RenderBox?;
        if (renderBox == null) return;

        final addSelectableWidget = ctx
            .read<CanvasProvider>()
            .selectableWidgetsProvider
            .addSelectableWidget;

        final offset =
            renderBox.globalToLocal(_.offset + const Offset(100, 100));
        data.cx = offset.dx / width;
        data.cy = offset.dy / height;
        if (data is SelectableImage) {
          data.scaleX = .2 / scale;
          final _ = Completer<void>();
          MemoryImage(data.data).resolve(ImageConfiguration.empty).addListener(
            ImageStreamListener((image, synchronousCall) {
              final imageHeight = image.image.height;
              final imageWidth = image.image.width;
              data.scaleY =
                  data.scaleX * (width / imageWidth) * (imageHeight / height);
              _.complete();
            }),
          );
          await _.future;
        }
        addSelectableWidget(data);
      },
      builder: (context, objects, items) {
        return IgnorePointer(
          child: Container(),
        );
      },
    );
  }
}
