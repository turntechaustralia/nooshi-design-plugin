import 'package:flayout/home/main/view.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TopCanvasGestureHandler extends StatelessWidget {
  final Widget canvas;

  const TopCanvasGestureHandler({
    Key? key,
    required this.canvas,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: context
          .watch<CanvasProvider>()
          .selectableWidgetsProvider
          .selectableWidgetOnTapAt,
      onSecondaryTap: context
          .watch<CanvasProvider>()
          .selectableWidgetsProvider
          .selectableWidgetOnSecendaryTapUpAt,
      onLongPress: context
          .watch<CanvasProvider>()
          .selectableWidgetsProvider
          .selectableWidgetOnSecendaryTapUpAt,
      child: Listener(
        onPointerSignal: (event) {
          if (event is! PointerScrollEvent) return;
          if (EditorView.isCTRLPressed) {
            final offset = event.scrollDelta.dy;
            if (offset < 0) {
              context.read<CanvasProvider>().sizeProvider.zoomIn(offset);
            } else {
              context.read<CanvasProvider>().sizeProvider.zoomOut(offset);
            }
          } else {
            final offset = -event.scrollDelta;
            context
                .read<CanvasProvider>()
                .sizeProvider
                .translateView(offset.dx, offset.dy);
          }
        },
        child: canvas,
      ),
    );
  }
}
