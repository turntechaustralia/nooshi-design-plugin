import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VisibleCanvasChild extends StatelessWidget {
  final bool isSelected;
  final double borderWidth;
  final SelectableWidget selectableWidget;

  const VisibleCanvasChild({
    Key? key,
    required this.isSelected,
    required this.borderWidth,
    required this.selectableWidget,
  }) : super(key: key);

  Widget _selectableWidgetBuilder({
    required final double width,
    required final double height,
  }) {
    final widget = selectableWidget;
    if (widget is st.SelectableText) {
      return widget.text(
        canvasHeight: height,
      );
    } else if (widget is SelectableImage) {
      return widget.image(
        canvasWidth: width,
        canvasHeight: height,
      );
    } else {
      return const ColoredBox(color: Colors.black);
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = context.watch<CanvasProvider>().sizeProvider.width;
    final height = context.watch<CanvasProvider>().sizeProvider.height;

    Widget child = _selectableWidgetBuilder(width: width, height: height);

    if (isSelected) {
      child = Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            width: borderWidth,
            color: Colors.blue,
          ),
        ),
        child: Opacity(
          opacity: 0,
          child: child,
        ),
      );
    } else {
      child = CompositedTransformTarget(
        link: selectableWidget.link,
        child: AnimatedOpacity(
          key: selectableWidget.key,
          curve: Curves.easeInOut,
          duration: const Duration(milliseconds: 350),
          opacity: selectableWidget.isVisible ? 1 : 0,
          child: child,
        ),
      );
    }

    final double widgetWidth;
    final double widgetHeight;

    if (selectableWidget is st.SelectableText) {
      final widget = selectableWidget as st.SelectableText;
      final size = widget.layout(canvasHeight: height);
      widgetWidth = size.width;
      widgetHeight = size.height;
    } else if (selectableWidget is SelectableImage) {
      final widget = selectableWidget as SelectableImage;
      widgetWidth = widget.scaleX * width;
      widgetHeight = widget.scaleY * height;
    } else {
      throw UnimplementedError();
    }

    final borderDiff = isSelected ? borderWidth : 0;
    return Positioned(
      top: (height * selectableWidget.cy - widgetHeight / 2) - borderDiff,
      left: (width * selectableWidget.cx - widgetWidth / 2) - borderDiff,
      child: Transform.rotate(
        angle: selectableWidget.rd,
        child: IgnorePointer(
          child: child,
        ),
      ),
    );
  }
}
