import 'package:flayout/home/canvas/canvas.dart';
import 'package:flayout/models/corner.dart';
import 'package:flayout/models/middle.dart';
import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

extension SelectedWidgetToolOverlay on CanvasArea {
  List<Widget> builder(final BuildContext context) {
    final selectedWidget = context
        .watch<CanvasProvider>()
        .selectableWidgetsProvider
        .selectedWidget;

    if (selectedWidget == null) return [];

    const padding = EdgeInsets.all(6);
    final isLocked = selectedWidget.isLocked;

    Widget lockBuilder({
      required final Widget child,
      required final MouseCursor cursor,
    }) {
      return AnimatedOpacity(
        curve: Curves.easeInOut,
        opacity: isLocked ? 0 : 1,
        duration: const Duration(milliseconds: 350),
        child: IgnorePointer(
          ignoring: isLocked,
          child: MouseRegion(
            cursor: isLocked ? SystemMouseCursors.basic : cursor,
            child: child,
          ),
        ),
      );
    }

    final List<Widget> result = [];

    result.add(
      (() {
        final MouseCursor cursor;
        final r = selectedWidget.r;
        if ((r > 45 && r < 135) || (r > 225 && r < 315)) {
          cursor = SystemMouseCursors.resizeUpDown;
        } else {
          cursor = SystemMouseCursors.resizeLeftRight;
        }
        return CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(0, -12),
          followerAnchor: Alignment.bottomCenter,
          targetAnchor: Alignment.topCenter,
          child: lockBuilder(
            cursor: cursor,
            child: GestureDetector(
              onPanUpdate: context
                  .watch<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onWidgetRotationDrag,
              child: SizedBox.fromSize(
                size: const Size.square(20),
                child: const Material(
                  color: Colors.blue,
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.rotate_left_rounded,
                    color: Colors.white,
                    size: 18,
                  ),
                ),
              ),
            ),
          ),
        );
      })(),
    );

    if (selectedWidget is SelectableImage) {
      MouseCursor _cornerCursor(final double difference) {
        final degrees = (selectedWidget.r + difference).remainder(360);
        if (degrees < 45 || degrees > 315) {
          return SystemMouseCursors.resizeUpRight;
        } else if (degrees < 135) {
          return SystemMouseCursors.resizeUpLeft;
        } else if (degrees < 225) {
          return SystemMouseCursors.resizeDownLeft;
        } else {
          return SystemMouseCursors.resizeDownRight;
        }
      }

      MouseCursor _centerCursor(final double difference) {
        final degrees = (selectedWidget.r + difference).remainder(360);
        if ((degrees > 45 && degrees < 135) ||
            (degrees > 225 && degrees < 315)) {
          return SystemMouseCursors.resizeLeftRight;
        } else {
          return SystemMouseCursors.resizeUpDown;
        }
      }

      result.addAll([
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(-1, 0),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.centerLeft,
          child: lockBuilder(
            cursor: _centerCursor(90),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageResize(details: details, middle: Middle.left),
              child: Container(
                width: 6,
                height: 32,
                margin: padding,
                child: const Material(
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(1, 0),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.centerRight,
          child: lockBuilder(
            cursor: _centerCursor(90),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageResize(details: details, middle: Middle.right),
              child: Container(
                width: 6,
                height: 32,
                margin: padding,
                child: const Material(
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(0, -1),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.topCenter,
          child: lockBuilder(
            cursor: _centerCursor(0),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onVerticalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageResize(details: details, middle: Middle.top),
              child: Container(
                width: 32,
                height: 6,
                margin: padding,
                child: const Material(
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(0, 1),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.bottomCenter,
          child: lockBuilder(
            cursor: _centerCursor(0),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onVerticalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageResize(details: details, middle: Middle.bottom),
              child: Container(
                width: 32,
                height: 6,
                margin: padding,
                child: const Material(
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(1, 1),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.bottomRight,
          child: lockBuilder(
            cursor: _cornerCursor(270),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageRescale(details: details, corner: Corner.bottomRight),
              child: Padding(
                padding: padding,
                child: SizedBox.fromSize(
                  size: const Size.square(12),
                  child: const Material(
                    color: Colors.blue,
                    shape: CircleBorder(),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(-1, 1),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.bottomLeft,
          child: lockBuilder(
            cursor: _cornerCursor(180),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageRescale(details: details, corner: Corner.bottomLeft),
              child: Padding(
                padding: padding,
                child: SizedBox.fromSize(
                  size: const Size.square(12),
                  child: const Material(
                    color: Colors.blue,
                    shape: CircleBorder(),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(-1, -1),
          followerAnchor: Alignment.center,
          child: lockBuilder(
            cursor: _cornerCursor(90),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageRescale(details: details, corner: Corner.topLeft),
              child: Padding(
                padding: padding,
                child: SizedBox.fromSize(
                  size: const Size.square(12),
                  child: const Material(
                    color: Colors.blue,
                    shape: CircleBorder(),
                  ),
                ),
              ),
            ),
          ),
        ),
        CompositedTransformFollower(
          link: selectedWidget.link,
          offset: const Offset(1, -1),
          followerAnchor: Alignment.center,
          targetAnchor: Alignment.topRight,
          child: lockBuilder(
            cursor: _cornerCursor(0),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onHorizontalDragUpdate: (details) => context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onImageRescale(details: details, corner: Corner.topRight),
              child: Padding(
                padding: padding,
                child: SizedBox.fromSize(
                  size: const Size.square(12),
                  child: const Material(
                    color: Colors.blue,
                    shape: CircleBorder(),
                  ),
                ),
              ),
            ),
          ),
        ),
      ]);
    }

    return result;
  }
}
