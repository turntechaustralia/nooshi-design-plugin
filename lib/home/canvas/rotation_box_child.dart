import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RotationBoxCanvasChild extends StatelessWidget {
  final SelectableWidget selectableWidget;

  const RotationBoxCanvasChild({
    Key? key,
    required this.selectableWidget,
  }) : super(key: key);

  Widget _selectableWidgetBuilder({
    required final double width,
    required final double height,
  }) {
    final widget = selectableWidget;
    if (widget is st.SelectableText) {
      return widget.text(
        canvasHeight: height,
      );
    } else if (widget is SelectableImage) {
      return widget.image(
        canvasWidth: width,
        canvasHeight: height,
      );
    } else {
      return const ColoredBox(color: Colors.black);
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = context.watch<CanvasProvider>().sizeProvider.width;
    final height = context.watch<CanvasProvider>().sizeProvider.height;

    final double widgetWidth;
    final double widgetHeight;

    if (selectableWidget is st.SelectableText) {
      final widget = selectableWidget as st.SelectableText;
      final size = widget.layout(canvasHeight: height);
      widgetWidth = size.width;
      widgetHeight = size.height;
    } else if (selectableWidget is SelectableImage) {
      final widget = selectableWidget as SelectableImage;
      widgetWidth = widget.scaleX * width;
      widgetHeight = widget.scaleY * height;
    } else {
      throw UnimplementedError();
    }

    return Positioned(
      top: height * selectableWidget.cy - widgetHeight / 2,
      left: width * selectableWidget.cx - widgetWidth / 2,
      child: IgnorePointer(
        key: selectableWidget.contextKey,
        child: Opacity(
          opacity: 0,
          child: _selectableWidgetBuilder(
            width: width,
            height: height,
          ),
        ),
      ),
    );
  }
}
