import 'package:flayout/home/canvas/rotation_box_child.dart';
import 'package:flayout/home/canvas/visible_child.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/key_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InteractiveCanvasSection extends StatelessWidget {
  const InteractiveCanvasSection({Key? key}) : super(key: key);

  List<Widget> _getVisibleChildren(final BuildContext context) {
    const borderWidth = 2.0;
    const border = Positioned(
      top: -borderWidth,
      left: -borderWidth,
      right: -borderWidth,
      bottom: -borderWidth,
      child: IgnorePointer(
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.fromBorderSide(
              BorderSide(color: Colors.blue, width: borderWidth),
            ),
          ),
        ),
      ),
    );

    final selectedWidget = context
        .watch<CanvasProvider>()
        .selectableWidgetsProvider
        .selectedWidget;

    return [
      ...context
          .watch<CanvasProvider>()
          .selectableWidgetsProvider
          .selectableWidgets
          .map(
            (widget) => VisibleCanvasChild(
              isSelected: false,
              borderWidth: borderWidth,
              selectableWidget: widget,
            ),
          )
          .toList(),
      if (selectedWidget != null)
        VisibleCanvasChild(
          isSelected: true,
          borderWidth: borderWidth,
          selectableWidget: selectedWidget,
        ),
      border,
    ];
  }

  List<Widget> _getRotationBoxChildren(final BuildContext context) {
    return context
        .watch<CanvasProvider>()
        .selectableWidgetsProvider
        .selectableWidgets
        .map(
          (widget) => RotationBoxCanvasChild(
            selectableWidget: widget,
          ),
        )
        .toList();
  }

  List<Widget> _getChildren(final BuildContext context) {
    return [
      ..._getRotationBoxChildren(context),
      ..._getVisibleChildren(context),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final width = context.watch<CanvasProvider>().sizeProvider.width;
    final height = context.watch<CanvasProvider>().sizeProvider.height;

    return InteractiveViewer(
      constrained: false,
      scaleEnabled: false,
      onInteractionStart:
          context.watch<CanvasProvider>().sizeProvider.onScaleStart,
      onInteractionUpdate:
          context.watch<CanvasProvider>().sizeProvider.onScaleUpdate,
      boundaryMargin: const EdgeInsets.all(double.infinity),
      transformationController:
          context.watch<CanvasProvider>().sizeProvider.controller,
      child: RepaintBoundary(
        key: context.watch<KeyProvider>().canvasKey,
        child: Container(
          width: width,
          height: height,
          color: context.watch<CanvasProvider>().colorProvider.color,
          child: Stack(
            clipBehavior: Clip.none,
            children: _getChildren(context),
          ),
        ),
      ),
    );
  }
}
