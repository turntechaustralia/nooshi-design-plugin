import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ContextMenu extends StatelessWidget {
  final bool contextMenuVisibility;
  final SelectableWidget? selectedWidget;

  const ContextMenu({
    Key? key,
    required this.selectedWidget,
    required this.contextMenuVisibility,
  }) : super(key: key);

  Widget _inkBuilder({
    required final Widget child,
    required final String tooltip,
    final bool preferBelow = true,
    required final void Function() onTap,
    required final BorderRadius borderRadius,
  }) {
    return InkWell(
      borderRadius: borderRadius,
      onTap: onTap,
      child: Tooltip(
        message: tooltip,
        preferBelow: preferBelow,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: child,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    const borderRadius = BorderRadius.all(Radius.circular(4));

    return IgnorePointer(
      ignoring: !contextMenuVisibility,
      child: AnimatedOpacity(
        curve: Curves.easeInOut,
        opacity: contextMenuVisibility ? 1 : 0,
        duration: const Duration(milliseconds: 350),
        child: Material(
          elevation: 5,
          color: Colors.white,
          shape: const RoundedRectangleBorder(
            borderRadius: borderRadius,
            side: BorderSide(width: 0, color: Colors.grey),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _inkBuilder(
                    preferBelow: false,
                    tooltip:
                        (selectedWidget?.isLocked ?? true) ? 'Unlock' : 'Lock',
                    borderRadius: borderRadius,
                    onTap: context
                        .watch<CanvasProvider>()
                        .selectableWidgetsProvider
                        .changeIsWidgetLockedAt,
                    child: Icon(
                      (selectedWidget?.isLocked ?? true)
                          ? Icons.lock
                          : Icons.lock_open,
                    ),
                  ),
                  _inkBuilder(
                    preferBelow: false,
                    tooltip:
                        (selectedWidget?.isVisible ?? true) ? 'Hide' : 'Show',
                    borderRadius: borderRadius,
                    onTap: context
                        .watch<CanvasProvider>()
                        .selectableWidgetsProvider
                        .changeIsWidgetVisibleAt,
                    child: Icon(
                      (selectedWidget?.isVisible ?? true)
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                  ),
                  _inkBuilder(
                    preferBelow: false,
                    tooltip: 'Move up',
                    borderRadius: borderRadius,
                    onTap: context
                        .watch<CanvasProvider>()
                        .selectableWidgetsProvider
                        .onSelectableWidgetMoveUp,
                    child: const Icon(
                      Icons.keyboard_arrow_up_rounded,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _inkBuilder(
                    tooltip: 'Delete',
                    borderRadius: borderRadius,
                    onTap: context
                        .watch<CanvasProvider>()
                        .selectableWidgetsProvider
                        .removeSelectedWidget,
                    child: const Icon(
                      Icons.delete,
                      color: Colors.red,
                    ),
                  ),
                  _inkBuilder(
                    tooltip: 'Rotate 90°',
                    borderRadius: borderRadius,
                    onTap: context
                        .watch<CanvasProvider>()
                        .selectableWidgetsProvider
                        .onWidgetRotation,
                    child: const Icon(
                      Icons.rotate_90_degrees_ccw_rounded,
                    ),
                  ),
                  _inkBuilder(
                    tooltip: 'Move down',
                    borderRadius: borderRadius,
                    onTap: context
                        .watch<CanvasProvider>()
                        .selectableWidgetsProvider
                        .onSelectableWidgetMoveDown,
                    child: const Icon(
                      Icons.keyboard_arrow_down_rounded,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
