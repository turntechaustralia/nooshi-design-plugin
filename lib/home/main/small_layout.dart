part of 'view.dart';

extension _SmallLayout on EditorView {
  double _maxCanvasOffset(final BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return min(_sectionChooserPlusSectionWidth, width * .8);
  }

  AppBar _titleBar(final BuildContext context) {
    return AppBar(
      elevation: 0,
      toolbarHeight: 89,
      backgroundColor: Colors.white,
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(1),
        child: Container(
          height: 1,
          color: borderColor,
        ),
      ),
      leading: IconButton(
        color: Colors.red,
        icon: const Icon(Icons.menu_outlined),
        onPressed: () {
          final chosenSection =
              context.read<ToolboxProvider>().lastChosenSection;
          context.read<ToolboxProvider>().onSectionTapped(chosenSection);
        },
      ),
      // centerTitle: false,
      // title: Row(
      //   children: [
      //     Padding(
      //       padding: const EdgeInsetsDirectional.only(start: 12),
      //       child: Image.asset('assets/logo.png', height: 24),
      //     ),
      //   ],
      // ),
    );
  }

  Widget _actions(final BuildContext context) {
    return PositionedDirectional(
      top: 8,
      end: 16,
      child: Material(
        color: Colors.transparent,
        child: Row(
          children: [
            InkWell(
              customBorder: const CircleBorder(),
              onTap: () => context.read<KeyProvider>().previewImage(context),
              child: const Tooltip(
                message: 'Preview',
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Icon(
                    Icons.image_search_rounded,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            InkWell(
              customBorder: const CircleBorder(),
              onTap: context.watch<ToolboxProvider>().switchToolboxState,
              child: RotatedBox(
                quarterTurns:
                    context.watch<ToolboxProvider>().isToolboxOpen ? 2 : 0,
                child: const Padding(
                  padding: EdgeInsets.all(16),
                  child: Icon(
                    Icons.arrow_circle_down_rounded,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _sectionBuilder(final BuildContext context) {
    final chosenSection = context.watch<ToolboxProvider>().chosenSection;
    final Widget? section;
    switch (chosenSection) {
      case Section.image:
        section = const ImageSection();
        break;
      case Section.text:
        section = const TextSection();
        break;
      case Section.canvas:
        section = const CanvasSection();
        break;
      case null:
        section = null;
        break;
    }
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 32, 24, 24),
        child: SizedBox(
          width: 280,
          child: section,
        ),
      ),
    );
  }

  Widget _toolBoxBuilder(final BuildContext context) {
    final isToolboxShown = context
            .watch<CanvasProvider>()
            .selectableWidgetsProvider
            .selectedWidget !=
        null;
    return Material(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 32, 24, 24),
              child: AnimatedSwitcher(
                switchInCurve: Curves.easeInOut,
                switchOutCurve: Curves.easeInOut.flipped,
                duration: context.read<ToolboxProvider>().animationDuration,
                child: isToolboxShown ? const Toolbox() : const ReorderBox(),
              ),
            ),
          ),
          horizontalDivider,
        ],
      ),
    );
  }

  Widget _canvasAnimator(final BuildContext context) {
    final isSectionOpen = context.watch<ToolboxProvider>().isSectionOpen;

    Widget _stackArea() {
      final isToolboxOpen = context.watch<ToolboxProvider>().isToolboxOpen;

      return LayoutBuilder(
        builder: (context, constraints) {
          final topHeight = (constraints.maxHeight + 89) / 2;
          final bottomHeight = (constraints.maxHeight - 89) / 2;
          return Stack(
            fit: StackFit.expand,
            children: [
              Scaffold(
                appBar: _titleBar(context),
                body: AnimatedPadding(
                  duration: context.read<ToolboxProvider>().animationDuration,
                  curve: Curves.easeInOut,
                  padding:
                      EdgeInsets.only(top: isToolboxOpen ? bottomHeight : 0),
                  child: const CanvasArea(),
                ),
              ),
              AnimatedPositioned(
                duration: context.read<ToolboxProvider>().animationDuration,
                curve: Curves.easeInOut,
                top: isToolboxOpen ? 0 : -topHeight,
                left: 0,
                right: 0,
                height: topHeight,
                child: _toolBoxBuilder(context),
              ),
              _actions(context),
            ],
          );
        },
      );
    }

    return AnimatedPositionedDirectional(
      duration: context.read<ToolboxProvider>().animationDuration,
      curve: Curves.easeInOut,
      top: isSectionOpen ? 10 : 0,
      bottom: isSectionOpen ? 10 : 0,
      end: isSectionOpen ? -_maxCanvasOffset(context) : 0,
      start: isSectionOpen ? _maxCanvasOffset(context) : 0,
      child: isSectionOpen
          ? MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                onTap: () {
                  final chosenSection =
                      context.read<ToolboxProvider>().chosenSection!;
                  context
                      .read<ToolboxProvider>()
                      .onSectionTapped(chosenSection);
                },
                child: DecoratedBox(
                  position: DecorationPosition.foreground,
                  decoration: BoxDecoration(
                    border: Border.all(width: 0, color: Colors.grey),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: ImageFiltered(
                    imageFilter: ImageFilter.blur(
                      sigmaX: 1,
                      sigmaY: 1,
                      tileMode: TileMode.decal,
                    ),
                    child: AbsorbPointer(
                      child: _stackArea(),
                    ),
                  ),
                ),
              ),
            )
          : _stackArea(),
    );
  }

  Widget _sectionAreaBuilder(final BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.centerStart,
      child: Material(
        child: SizedBox(
          width: _maxCanvasOffset(context),
          child: Row(
            children: [
              _sectionChooserBuilder(context),
              divider,
              Expanded(
                child: _sectionBuilder(context),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _smallLayoutBuilder(final BuildContext context) {
    return Stack(
      children: [
        _sectionAreaBuilder(context),
        _canvasAnimator(context),
      ],
    );
  }
}
