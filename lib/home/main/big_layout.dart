part of 'view.dart';

extension _BigLayout on EditorView {
  AppBar _titleBar(final BuildContext context) {
    return AppBar(
      elevation: 0,
      toolbarHeight: 100,
      backgroundColor: Colors.black,
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
        ),
      ),
      centerTitle: true,
      title: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const InkWell(
                child: Padding(
                  padding: EdgeInsetsDirectional.only(start: 55),
                  child: Icon(
                    Icons.close,
                    color: Colors.blue,
                    size: 30,
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  context.read<KeyProvider>().previewImage(context);
                },
                child: const Padding(
                  padding: EdgeInsetsDirectional.only(start: 55),
                  child: Text(
                    "Preview",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
              ),
              const InkWell(
                child: Padding(
                  padding: EdgeInsetsDirectional.only(end: 65),
                  child: Icon(
                    Icons.check,
                    color: Colors.blue,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only( top :5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // const Padding(
                //   padding: EdgeInsetsDirectional.only(end: 32),
                //   child: Text(
                //     "Front Back",
                //     textAlign: TextAlign.center,
                //     style: TextStyle(
                //       color: Colors.blueAccent,
                //     ),
                //   ),
                // ),
                // const Padding(
                //   padding: EdgeInsetsDirectional.only(start: 18),
                //   child: Text(
                //     "icon",
                //     textAlign: TextAlign.center,
                //     style: TextStyle(
                //       color: Colors.blueAccent,
                //     ),
                //   ),
                // ),
                InkWell(
                  child: Padding(
                    padding: const EdgeInsetsDirectional.only(start: 30),
                    child: IconButton(
                      color: Colors.blueAccent,
                      icon: Icon(
                        context.watch<ToolboxProvider>().isToolboxOpen
                            ? Icons.menu
                            : Icons.menu_open,
                      ),
                      onPressed: context.watch<ToolboxProvider>().switchToolboxState,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _sectionBuilder(final BuildContext context) {
    final chosenSection = context.watch<ToolboxProvider>().chosenSection;
    final Widget? section;
    switch (chosenSection) {
      case Section.image:
        section = const ImageSection();
        break;
      case Section.text:
        section = const TextSection();
        break;
      case Section.canvas:
        section = const CanvasSection();
        break;
      case null:
        section = null;
        break;
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Material(
          color: Theme.of(context).canvasColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(24, 32, 24, 24),
            child: SizedBox(
              width: 280,
              child: section,
            ),
          ),
        ),
        divider,
      ],
    );
  }

  Widget _sectionAnimator(final BuildContext context) {
    return AnimatedPositioned(
      top: 0,
      bottom: 0,
      curve: Curves.easeInOut,
      duration: context.read<ToolboxProvider>().animationDuration,
      left: context.watch<ToolboxProvider>().isSectionOpen
          ? 0
          : -EditorView.toolbarPlusDividerWidth,
      child: _sectionBuilder(context),
    );
  }

  Widget _toolBoxBuilder(final BuildContext context) {
    return Material(
      color: Theme.of(context).canvasColor,
      child: Row(
        children: [
          divider,
          const Padding(
            padding: EdgeInsets.fromLTRB(24, 32, 24, 24),
            child: SizedBox(
              width: 280,
              child: ToolboxArea(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toolBoxAnimator(final BuildContext context) {
    return AnimatedPositioned(
      top: 0,
      bottom: 0,
      curve: Curves.easeInOut,
      duration: context.read<ToolboxProvider>().animationDuration,
      right: context.watch<ToolboxProvider>().isToolboxOpen
          ? 0
          : -EditorView.toolbarPlusDividerWidth,
      child: _toolBoxBuilder(context),
    );
  }

  Widget _bigLayoutBuilder(final BuildContext context) {
    return Scaffold(
      appBar: _titleBar(context),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: [
                const CanvasArea(),
                _sectionAnimator(context),
                _toolBoxAnimator(context),
              ],
            ),
          ),
          divider,
          _sectionChooserBuilder(context)
        ],
      ),
    );
  }
}
