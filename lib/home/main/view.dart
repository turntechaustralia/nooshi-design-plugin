import 'dart:math' show min;
import 'dart:ui';

import 'package:flayout/constants/colors.dart';
import 'package:flayout/home/canvas/canvas.dart';
import 'package:flayout/home/sections/canvas/canvas_button.dart';
import 'package:flayout/home/sections/canvas/canvas_section.dart';
import 'package:flayout/home/sections/image_section/image_button.dart';
import 'package:flayout/home/sections/image_section/image_section.dart';
import 'package:flayout/home/sections/text_section/text_button.dart';
import 'package:flayout/home/sections/text_section/text_section.dart';
import 'package:flayout/home/toolbox/reorder_box.dart';
import 'package:flayout/home/toolbox/toolbox_area.dart';
import 'package:flayout/home/toolbox/toolboxes/toolbox_selector.dart';
import 'package:flayout/models/sections.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/key_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

part 'big_layout.dart';
part 'small_layout.dart';

class EditorView extends StatefulWidget {
  static bool isCTRLPressed = false;
  static const toolbarPlusDividerWidth = 329.0;

  const EditorView({Key? key}) : super(key: key);

  double get _sectionChooserPlusSectionWidth => 419.0;

  Widget get divider => Container(width: 1, color: borderColor);

  Widget get horizontalDivider => Container(height: 1, color: borderColor);

  Widget _sectionChooserBuilder(final BuildContext context) {
    return Container(
      color: Colors.black,
      padding: const EdgeInsets.fromLTRB(16, 20, 16, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          TextSectionButton(),
          ImageSectionButton(),
          CanvasSectionButton(),
        ],
      ),
    );
  }

  Widget _mainBuilder(final BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final width = constraints.biggest.width;
        final isBigLayout = width >= 850;
        context
            .read<CanvasProvider>()
            .sizeProvider
            .setIsBigLayout(isBigLayout: isBigLayout);
        if (isBigLayout) {
          return _bigLayoutBuilder(context);
        } else {
          return _smallLayoutBuilder(context);
        }
      },
    );
  }

  @override
  State<EditorView> createState() => _EditorViewState();
}

class _EditorViewState extends State<EditorView> {
  late final FocusAttachment _nodeAttachment;
  late final FocusNode focus;

  @override
  void initState() {
    super.initState();
    focus = FocusNode(debugLabel: 'CTRL_Button');
    _nodeAttachment = focus.attach(
      context,
      onKey: (node, event) {
        setState(() => EditorView.isCTRLPressed = event.isControlPressed);
        return KeyEventResult.ignored;
      },
    );
    focus.requestFocus();
  }

  @override
  void dispose() {
    focus.dispose();
    super.dispose();
  }

  Widget get proccessing {
    return Material(
      color: Colors.grey.withOpacity(.5),
      child: const Center(
        child: Material(
          color: Colors.red,
          clipBehavior: Clip.hardEdge,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Proccessing...',
              style: TextStyle(color: Colors.black),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _nodeAttachment.reparent();
    final isProccessing = context.watch<KeyProvider>().isProccessing;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          widget._mainBuilder(context),
          if (isProccessing) proccessing,
        ],
      ),
    );
  }
}
