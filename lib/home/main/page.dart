import 'package:flayout/home/main/view.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/key_provider.dart';
import 'package:flayout/providers/loaded_image_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditorPage extends StatelessWidget {
  const EditorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => KeyProvider()),
        ChangeNotifierProvider(create: (_) => CanvasProvider()),
        ChangeNotifierProvider(create: (_) => ToolboxProvider()),
        ChangeNotifierProvider(create: (_) => LoadedImageProvider()),
      ],
      child: const EditorView(),
    );
  }
}
