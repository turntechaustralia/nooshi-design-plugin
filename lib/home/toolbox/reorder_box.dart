import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ReorderBox extends StatelessWidget {
  const ReorderBox({Key? key}) : super(key: key);

  Widget _textPreviewBuilder(final st.SelectableText widget) {
    final text = widget.data.text.trim();
    final String preview;
    if (text.isEmpty) {
      preview = 'T';
    } else if (text.length < 3) {
      preview = text.substring(0, text.length);
    } else {
      preview = text.substring(0, 3);
    }
    final style = widget.textStyle.copyWith(fontSize: 14, color: Colors.black);
    return Text(
      preview,
      style: style,
    );
  }

  Widget _imagePreviewBuilder(final SelectableImage widget) {
    return widget.image(canvasWidth: 40 / widget.scaleX, applySettings: false);
  }

  Widget _previewBuilder(final SelectableWidget widget) {
    if (widget is st.SelectableText) {
      return _textPreviewBuilder(widget);
    } else if (widget is SelectableImage) {
      return _imagePreviewBuilder(widget);
    } else {
      return Container(
        width: 40,
        color: Colors.black,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text(
          'Reorder Box',
          style: TextStyle(
            height: 1.2,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 12),
        Expanded(
          child: ReorderableListView.builder(
            shrinkWrap: true,
            buildDefaultDragHandles: false,
            scrollController:
                context.watch<ToolboxProvider>().reorderScrollController,
            itemCount: context
                .watch<CanvasProvider>()
                .selectableWidgetsProvider
                .selectableWidgetsCount,
            itemBuilder: (ctx, index) {
              final widget = context
                  .watch<CanvasProvider>()
                  .selectableWidgetsProvider
                  .widgetAt(index);
              return Row(
                key: widget.key,
                children: [
                  Expanded(
                    child: MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () => context
                            .read<CanvasProvider>()
                            .selectableWidgetsProvider
                            .selectableWidgetOnTapAt(index),
                        child: Container(
                          height: 32,
                          width: double.infinity,
                          alignment: Alignment.centerLeft,
                          margin: const EdgeInsets.symmetric(vertical: 8),
                          child: _previewBuilder(widget),
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(widget.isLocked ? Icons.lock : Icons.lock_open),
                    onPressed: () => context
                        .read<CanvasProvider>()
                        .selectableWidgetsProvider
                        .changeIsWidgetLockedAt(index),
                  ),
                  IconButton(
                    icon: Icon(
                      widget.isVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                    onPressed: () => context
                        .read<CanvasProvider>()
                        .selectableWidgetsProvider
                        .changeIsWidgetVisibleAt(index),
                  ),
                  ReorderableDragStartListener(
                    index: index,
                    child: const MouseRegion(
                      cursor: SystemMouseCursors.resizeUpDown,
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: Icon(Icons.drag_handle),
                      ),
                    ),
                  )
                ],
              );
            },
            onReorder: (final int oldOrder, int newOrder) {
              if (newOrder > oldOrder) newOrder--;
              context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .onSelectableWidgetReordered(oldOrder, newOrder);
            },
          ),
        ),
      ],
    );
  }
}
