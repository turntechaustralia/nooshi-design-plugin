import 'package:flayout/home/toolbox/reorder_box.dart';
import 'package:flayout/home/toolbox/toolboxes/toolbox_selector.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ToolboxArea extends StatelessWidget {
  const ToolboxArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final height = constraints.maxHeight;
        final toolboxHeight = height * 2 / 3;
        final isToolboxShown = context
                .watch<CanvasProvider>()
                .selectableWidgetsProvider
                .selectedWidget !=
            null;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            AnimatedContainer(
              curve: Curves.easeInOut,
              duration: const Duration(milliseconds: 350),
              padding: EdgeInsets.only(bottom: isToolboxShown ? 12 : 0),
              height: isToolboxShown ? toolboxHeight : 0,
              child: const Toolbox(),
            ),
            const Expanded(
              child: ReorderBox(),
            ),
          ],
        );
      },
    );
  }
}
