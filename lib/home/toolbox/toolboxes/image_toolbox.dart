import 'package:flayout/components/slidable_settable_value.dart';
import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ImageToolbox extends StatelessWidget {
  final SelectableImage selectableImage;

  const ImageToolbox({
    Key? key,
    required this.selectableImage,
  }) : super(key: key);

  Widget _fitBuilder(final BuildContext context) {
    const boxFit = [
      BoxFit.cover,
      BoxFit.fill,
    ];
    return Row(
      children: [
        const Expanded(
          child: Text(
            'Fit',
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        DropdownButtonHideUnderline(
          child: DropdownButton<BoxFit>(
            items: boxFit.map((fit) {
              return DropdownMenuItem(
                value: fit,
                alignment: Alignment.center,
                child: Text(describeEnum(fit)),
              );
            }).toList(),
            alignment: Alignment.center,
            value: selectableImage.fit,
            onChanged: (fit) {
              context
                  .read<CanvasProvider>()
                  .selectableWidgetsProvider
                  .setImageNewSettings(
                    image: selectableImage,
                    settings: SelectableImageSettings(fit: fit),
                  );
            },
          ),
        ),
      ],
    );
  }

  Widget _alignmentBuilder(final BuildContext context) {
    final isShown = selectableImage.fit != BoxFit.fill;
    const alignment = [
      Alignment.topCenter,
      Alignment.centerRight,
      Alignment.center,
      Alignment.bottomCenter,
      Alignment.centerLeft,
    ];
    return AnimatedSize(
      curve: Curves.easeInOut,
      duration: const Duration(milliseconds: 350),
      child: isShown
          ? Row(
              children: [
                const Expanded(
                  child: Text(
                    'Alignment',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
                DropdownButtonHideUnderline(
                  child: DropdownButton<Alignment>(
                    items: alignment.map((alignment) {
                      return DropdownMenuItem(
                        value: alignment,
                        alignment: Alignment.center,
                        child: Text(describeEnum(alignment)),
                      );
                    }).toList(),
                    alignment: Alignment.center,
                    value: selectableImage.alignment,
                    onChanged: (alignment) {
                      context
                          .read<CanvasProvider>()
                          .selectableWidgetsProvider
                          .setImageNewSettings(
                            image: selectableImage,
                            settings:
                                SelectableImageSettings(alignment: alignment),
                          );
                    },
                  ),
                ),
              ],
            )
          : const SizedBox(),
    );
  }

  Widget _cornerRadiusSliderBuilder(final BuildContext context) {
    return SlidableSettableValue(
      title: 'Corner Radius',
      minValue: 0,
      maxValue: 50,
      isLowValue: true,
      value: selectableImage.cr,
      valueChanged: (cr) => context
          .read<CanvasProvider>()
          .selectableWidgetsProvider
          .setImageNewSettings(
            image: selectableImage,
            settings: SelectableImageSettings(cr: cr),
          ),
    );
  }

  Widget _deleteButtonBuilder(final BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Material(
        shape: const CircleBorder(),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          onTap: context
              .watch<CanvasProvider>()
              .selectableWidgetsProvider
              .removeSelectedWidget,
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(
              Icons.delete,
              color: Colors.red,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: context.watch<ToolboxProvider>().scrollController,
      children: [
        _fitBuilder(context),
        _alignmentBuilder(context),
        _cornerRadiusSliderBuilder(context),
        _deleteButtonBuilder(context),
      ],
    );
  }
}
