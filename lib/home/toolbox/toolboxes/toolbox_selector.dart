import 'package:flayout/home/toolbox/toolboxes/image_toolbox.dart';
import 'package:flayout/home/toolbox/toolboxes/text_toolbox.dart';
import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Toolbox extends StatelessWidget {
  static SelectableWidget? _selectedWidget;

  const Toolbox({Key? key}) : super(key: key);

  Widget _getToolboxTextBasedOnSelectedWidget() {
    final String text;
    switch (_selectedWidget.runtimeType) {
      case Null:
        return const SizedBox();
      case st.SelectableText:
        text = 'Text Toolbox';
        break;
      case SelectableImage:
        text = 'Image Toolbox';
        break;
      default:
        throw UnimplementedError();
    }
    return Text(
      text,
      style: const TextStyle(
        height: 1.2,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _getToolboxBasedOnSelectedWidget(final BuildContext context) {
    switch (_selectedWidget.runtimeType) {
      case Null:
        return const SizedBox();
      case SelectableImage:
        return ImageToolbox(
          selectableImage: _selectedWidget! as SelectableImage,
        );
      case st.SelectableText:
        return TextToolbox(
          selectableText: _selectedWidget! as st.SelectableText,
        );
    }
    throw UnimplementedError();
  }

  @override
  Widget build(BuildContext context) {
    final selectedWidget = context
        .watch<CanvasProvider>()
        .selectableWidgetsProvider
        .selectedWidget;
    if (selectedWidget != _selectedWidget) {
      context.read<ToolboxProvider>().resetScrollController();
    }
    _selectedWidget = selectedWidget ?? _selectedWidget;
    final child = _getToolboxBasedOnSelectedWidget(context);
    return Stack(
      children: [
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: _getToolboxTextBasedOnSelectedWidget(),
        ),
        Positioned(
          top: 32,
          left: 0,
          right: 0,
          bottom: 0,
          child: child,
        ),
      ],
    );
  }
}
