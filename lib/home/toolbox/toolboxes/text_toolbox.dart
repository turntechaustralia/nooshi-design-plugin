import 'package:flayout/components/color_picker.dart';
import 'package:flayout/components/slidable_settable_value.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TextToolbox extends StatelessWidget {
  static const _borderColor = Colors.grey;
  final st.SelectableText selectableText;

  const TextToolbox({
    Key? key,
    required this.selectableText,
  }) : super(key: key);

  void setFontSize({
    required final double fontSize,
    required final BuildContext context,
  }) {
    final settings = st.SelectableTextSettings(fontSize: fontSize);
    context.read<CanvasProvider>().selectableWidgetsProvider.setTextNewSettings(
          text: selectableText,
          settings: settings,
        );
  }

  void setColor({
    required final Color color,
    required final BuildContext context,
  }) {
    final settings = st.SelectableTextSettings(color: color);
    context.read<CanvasProvider>().selectableWidgetsProvider.setTextNewSettings(
          text: selectableText,
          settings: settings,
        );
  }

  Widget verticalBorder() {
    return Container(
      width: 1,
      color: _borderColor,
    );
  }

  Widget _deleteButtonBuilder(final BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 42, vertical: 6),
      child: Align(
        alignment: Alignment.centerRight,
        child: Material(
          shape: const CircleBorder(),
          clipBehavior: Clip.hardEdge,
          child: InkWell(
            onTap: context
                .watch<CanvasProvider>()
                .selectableWidgetsProvider
                .removeSelectedWidget,
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(
                Icons.delete,
                color: Colors.red,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _textStyleBuilder(final BuildContext context) {
    final isBolded = selectableText.isBolded;
    final isItalic = selectableText.isItalic;
    final isUnderlined = selectableText.isUnderlined;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Style'),
          const SizedBox(width: 12),
          Container(
            width: 80,
            height: 27,
            margin: const EdgeInsetsDirectional.only(end: 24),
            child: Material(
              clipBehavior: Clip.hardEdge,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: const BorderSide(color: _borderColor),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Material(
                      color:
                          isBolded ? Colors.grey.shade300 : Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          final newFontWeight =
                              isBolded ? FontWeight.normal : FontWeight.bold;
                          final settings = st.SelectableTextSettings(
                            fontWeight: newFontWeight,
                          );
                          context
                              .read<CanvasProvider>()
                              .selectableWidgetsProvider
                              .setTextNewSettings(
                                text: selectableText,
                                settings: settings,
                              );
                        },
                        child: const Center(
                          child: Text(
                            'B',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ),
                  verticalBorder(),
                  Expanded(
                    child: Material(
                      color:
                          isItalic ? Colors.grey.shade300 : Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          final newFontStyle =
                              isItalic ? FontStyle.normal : FontStyle.italic;
                          final settings = st.SelectableTextSettings(
                            fontStyle: newFontStyle,
                          );
                          context
                              .read<CanvasProvider>()
                              .selectableWidgetsProvider
                              .setTextNewSettings(
                                text: selectableText,
                                settings: settings,
                              );
                        },
                        child: const Center(
                          child: Text(
                            'I',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                        ),
                      ),
                    ),
                  ),
                  verticalBorder(),
                  Expanded(
                    child: Material(
                      color: isUnderlined
                          ? Colors.grey.shade300
                          : Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          final settings = st.SelectableTextSettings(
                            isUnderlined: !isUnderlined,
                          );
                          context
                              .read<CanvasProvider>()
                              .selectableWidgetsProvider
                              .setTextNewSettings(
                                text: selectableText,
                                settings: settings,
                              );
                        },
                        child: const Center(
                          child: Text(
                            'U',
                            style:
                                TextStyle(decoration: TextDecoration.underline),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _textAlignBuilder(final BuildContext context) {
    final isLeftAligned = selectableText.isLeftAligned;
    final isRightAligned = selectableText.isRightAligned;
    final isCenterAligned = selectableText.isCenterAligned;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Align'),
          const SizedBox(width: 12),
          Container(
            width: 80,
            height: 27,
            margin: const EdgeInsetsDirectional.only(end: 24),
            child: Material(
              clipBehavior: Clip.hardEdge,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: const BorderSide(color: _borderColor),
              ),
              child: IconTheme(
                data: const IconThemeData(size: 14),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Material(
                        color: isLeftAligned
                            ? Colors.grey.shade300
                            : Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            const settings = st.SelectableTextSettings(
                              textAlign: TextAlign.left,
                            );
                            context
                                .read<CanvasProvider>()
                                .selectableWidgetsProvider
                                .setTextNewSettings(
                                  text: selectableText,
                                  settings: settings,
                                );
                          },
                          child: const Center(
                            child: Icon(Icons.format_align_left),
                          ),
                        ),
                      ),
                    ),
                    verticalBorder(),
                    Expanded(
                      child: Material(
                        color: isCenterAligned
                            ? Colors.grey.shade300
                            : Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            const settings = st.SelectableTextSettings(
                              textAlign: TextAlign.center,
                            );
                            context
                                .read<CanvasProvider>()
                                .selectableWidgetsProvider
                                .setTextNewSettings(
                                  text: selectableText,
                                  settings: settings,
                                );
                          },
                          child: const Center(
                            child: Icon(Icons.format_align_center),
                          ),
                        ),
                      ),
                    ),
                    verticalBorder(),
                    Expanded(
                      child: Material(
                        color: isRightAligned
                            ? Colors.grey.shade300
                            : Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            const settings = st.SelectableTextSettings(
                              textAlign: TextAlign.right,
                            );
                            context
                                .read<CanvasProvider>()
                                .selectableWidgetsProvider
                                .setTextNewSettings(
                                  text: selectableText,
                                  settings: settings,
                                );
                          },
                          child: const Center(
                            child: Icon(Icons.format_align_right),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: context.watch<ToolboxProvider>().scrollController,
      children: [
        SlidableSettableValue(
          title: 'Font Size',
          minValue: 2,
          isLowValue: true,
          maxValue: double.infinity,
          value: selectableText.textStyle.fontSize!,
          valueChanged: (_) => setFontSize(context: context, fontSize: _),
        ),
        _textStyleBuilder(context),
        _textAlignBuilder(context),
        ColorPicker(
          title: 'Text Color',
          pickedColor: selectableText.textStyle.color!,
          pickedColorChanged: (_) => setColor(color: _, context: context),
        ),
        _deleteButtonBuilder(context),
        TextField(
          maxLines: null,
          controller: selectableText.data,
          focusNode: selectableText.textFocusNode,
          decoration: const InputDecoration(border: OutlineInputBorder()),
        ),
      ],
    );
  }
}
