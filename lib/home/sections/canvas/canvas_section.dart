import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:provider/provider.dart';

class CanvasSection extends StatelessWidget {
  const CanvasSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: context.watch<ToolboxProvider>().scrollController,
      children: [
        ExpansionTile(
          backgroundColor: Colors.black,
          tilePadding: EdgeInsets.zero,
          textColor: Colors.black,
          iconColor: Colors.black,
          childrenPadding: const EdgeInsets.symmetric(horizontal: 12),
          title: const Text(
            'Background Color',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          expandedCrossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ColorPicker(
              portraitOnly: true,
              pickerColor: context.watch<CanvasProvider>().colorProvider.color,
              onColorChanged:
                  context.watch<CanvasProvider>().colorProvider.setColor,
            ),
          ],
        ),
      ],
    );
  }
}
