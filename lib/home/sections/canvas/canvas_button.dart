 import 'package:flayout/home/sections/section_button.dart';
import 'package:flayout/models/sections.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CanvasSectionButton extends StatelessWidget {
  const CanvasSectionButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SectionButton(
      title: 'CANVAS',
      icon: Icons.settings_rounded,
      color: Colors.blueAccent,
      isChosen: context.watch<ToolboxProvider>().isSectionCanvas,
      onTap: () =>
          context.read<ToolboxProvider>().onSectionTapped(Section.canvas),
    );
  }
}
