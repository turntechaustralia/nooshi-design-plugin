import 'package:flutter/material.dart';

class SectionButton extends StatelessWidget {
  final Color color;
  final String title;
  final IconData icon;
  final bool isChosen;
  final void Function() onTap;

  const SectionButton({
    Key? key,
    required this.color,
    required this.title,
    required this.icon,
    required this.isChosen,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: SizedBox(
          height: 80,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox.fromSize(
                size: const Size.square(30),
                child: Material(
                  color: isChosen ? Colors.black : Colors.black,
                  shape: const CircleBorder(),
                  clipBehavior: Clip.hardEdge,
                  child: Icon(
                    icon,
                    size: 30,
                    color: isChosen ? Colors.blueAccent : Colors.blueAccent,
                  ),
                ),
              ),
              Text(
                title,
                style: const TextStyle(
                  height:2,
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
