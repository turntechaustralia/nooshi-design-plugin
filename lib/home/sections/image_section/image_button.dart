import 'package:flayout/home/sections/section_button.dart';
import 'package:flayout/models/sections.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ImageSectionButton extends StatelessWidget {
  const ImageSectionButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SectionButton(
      title: 'Images',
      icon: Icons.image,
      color: Colors.blueAccent,
      isChosen: context.watch<ToolboxProvider>().isSectionImage,
      onTap: () =>
          context.read<ToolboxProvider>().onSectionTapped(Section.image),
    );
  }
}
