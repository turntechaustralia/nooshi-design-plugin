import 'package:flayout/constants/colors.dart';
import 'package:flayout/providers/loaded_image_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UploadImage extends StatefulWidget {
  const UploadImage({Key? key}) : super(key: key);

  @override
  _UploadImageState createState() => _UploadImageState();
}

class _UploadImageState extends State<UploadImage> {
  bool _isHovered = false;

  void _unHovered([dynamic _]) {
    setState(() {
      _isHovered = false;
    });
  }

  void _hovered([dynamic _]) {
    setState(() {
      _isHovered = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: context.watch<LoadedImageProvider>().addImage,
      child: MouseRegion(
        onEnter: _hovered,
        onExit: _unHovered,
        cursor: SystemMouseCursors.click,
        child: AnimatedContainer(
          height: 140,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: _isHovered ? Border.all() : Border.all(color: borderColor),
          ),
          duration: const Duration(milliseconds: 200),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Icon(
                Icons.upload_outlined,
                color: Colors.black,
              ),
              SizedBox(height: 4),
              Text(
                'Upload logo or image',
                style: TextStyle(
                  height: 1.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
