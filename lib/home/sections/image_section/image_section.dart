import 'package:flayout/constants/colors.dart';
import 'package:flayout/constants/platform.dart';
import 'package:flayout/home/sections/image_section/upload_image.dart';
import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/loaded_image_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ImageSection extends StatelessWidget {
  const ImageSection({Key? key}) : super(key: key);

  Widget _loadingImageBuilder({
    required final BuildContext context,
    required final String url,
  }) {
    final progress =
        context.watch<LoadedImageProvider>().loadingImageProgressFor(url);
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(color: borderColor),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Center(
        child: CircularProgressIndicator.adaptive(
          value: progress,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final loadingImageKeys =
        context.watch<LoadedImageProvider>().loadingImageKeys..sort();
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'Images',
            style: TextStyle(
              height: 1.2,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 12),
          const Text(
            'Once you’ve uploaded your image, click and drag it onto your design.',
            style: TextStyle(height: 1.5),
          ),
          DefaultTextStyle(
            style: const TextStyle(
              height: 1.5,
              color: Colors.black,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: ExpansionTile(
                tilePadding: EdgeInsets.zero,
                textColor: Colors.black,
                iconColor: Colors.black,
                title: const Text(
                  'Accepted formats',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                expandedCrossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'You can upload files with the following extensions:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: Wrap(
                      spacing: 8,
                      runSpacing: 8,
                      children: const [
                        Text('.jpg,'),
                        Text('.jpeg,'),
                        Text('.bmp,'),
                        Text('.png.'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 12),
          const UploadImage(),
          GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 12,
              crossAxisSpacing: 12,
            ),
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(vertical: 12),
            itemCount: context.watch<LoadedImageProvider>().imageCount,
            itemBuilder: (_, index) {
              final loadedImageCount =
                  context.read<LoadedImageProvider>().loadedImageCount;
              if (index < loadedImageCount) {
                return _DraggableImage(index: index);
              } else {
                return _loadingImageBuilder(
                  context: context,
                  url: loadingImageKeys[index - loadedImageCount],
                );
              }
            },
          ),
        ],
      ),
    );
  }
}

class _DraggableImage extends StatefulWidget {
  final int index;

  const _DraggableImage({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  __DraggableImageState createState() => __DraggableImageState();
}

class __DraggableImageState extends State<_DraggableImage> {
  bool isHovered = false;

  Widget child({required final bool isHovered}) {
    return Stack(
      children: [
        Draggable<SelectableWidget>(
          data: SelectableImage(
            data:
                context.read<LoadedImageProvider>().loadedImageAt(widget.index),
            extension: context
                .read<LoadedImageProvider>()
                .loadedImageExtAt(widget.index),
          ),
          maxSimultaneousDrags: 1,
          onDragStarted: () {
            final canvasProvider = context.read<CanvasProvider>();
            canvasProvider.selectableWidgetsProvider.selectableWidgetOnTapAt();
            final isSmallLayout = MediaQuery.of(context).size.width < 850;
            if (isSmallLayout) {
              final toolboxProvider = context.read<ToolboxProvider>();
              toolboxProvider.onSectionTapped();
            }
          },
          feedback: Container(
            constraints: const BoxConstraints(
              maxWidth: 200,
              maxHeight: 200,
            ),
            child: Image.memory(
              context.read<LoadedImageProvider>().loadedImageAt(widget.index),
              width: 200,
              height: 200,
              fit: BoxFit.contain,
            ),
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              border: Border.all(color: borderColor),
              borderRadius: BorderRadius.circular(8),
            ),
            child: Align(
              child: Image.memory(
                context.read<LoadedImageProvider>().loadedImageAt(widget.index),
                width: 134,
                height: 134,
              ),
            ),
          ),
        ),
        if (isHovered)
          Positioned(
            top: 4,
            right: 4,
            child: Material(
              color: Colors.red,
              shape: const CircleBorder(),
              clipBehavior: Clip.hardEdge,
              child: InkWell(
                onTap: () => context
                    .read<LoadedImageProvider>()
                    .removeLoadedImageAt(widget.index),
                child: const Padding(
                  padding: EdgeInsets.all(4),
                  child: Icon(
                    Icons.delete,
                    size: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (isTargetMobile) return child(isHovered: true);
    return MouseRegion(
      onExit: (_) => setState(() => isHovered = false),
      onEnter: (_) => setState(() => isHovered = true),
      child: child(isHovered: isHovered),
    );
  }
}
