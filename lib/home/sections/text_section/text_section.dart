import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TextSection extends StatelessWidget {
  static final _controller = TextEditingController();

  const TextSection({Key? key}) : super(key: key);

  void addText(final BuildContext context) {
    final string = _controller.text;
    _controller.clear();
    if (string.trim().isEmpty) return;
    final canvasProvider = context.read<CanvasProvider>();
    final addSelectableText =
        canvasProvider.selectableWidgetsProvider.addSelectableWidget;
    final text = st.SelectableText(
      data: TextEditingController(text: string),
    );
    text.copyWith(
      const st.SelectableTextSettings(
        fontSize: 32,
        color: Colors.black,
      ),
    );
    addSelectableText(text);
    final isSmallLayout = MediaQuery.of(context).size.width < 850;
    if (isSmallLayout) {
      final toolboxProvider = context.read<ToolboxProvider>();
      toolboxProvider.onSectionTapped();
      if (!toolboxProvider.isToolboxOpen) {
        Future.delayed(
          toolboxProvider.animationDuration,
          toolboxProvider.switchToolboxState,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TextField(
            maxLines: null,
            controller: _controller,
            decoration: const InputDecoration(
              hintText: 'Enter text here...',
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 16),
          TextButton(
            onPressed: () => addText(context),
            child: const Text('add text'),
          ),
        ],
      ),
    );
  }
}
