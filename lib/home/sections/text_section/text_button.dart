import 'package:flayout/home/sections/section_button.dart';
import 'package:flayout/models/sections.dart';
import 'package:flayout/providers/toolbox_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TextSectionButton extends StatelessWidget {
  const TextSectionButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SectionButton(
      title: 'Text',
      icon: Icons.title_rounded,
      color: Colors.blueAccent,
      isChosen: context.watch<ToolboxProvider>().isSectionText,
      onTap: () =>
          context.read<ToolboxProvider>().onSectionTapped(Section.text),
    );
  }
}
