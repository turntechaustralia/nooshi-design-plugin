import 'package:flayout/app/navigation.dart';
import 'package:flayout/constants/theme.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

class EditorApp extends StatelessWidget {
  final List<VRouteElement> _routerDelegate =
      FlayoutNavigation.instance.routerDelegate;

  EditorApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VRouter(
      title: 'FLAYOUT',
      theme: appLightTheme,
      routes: _routerDelegate,
      themeMode: ThemeMode.light,
      mode: VRouterMode.history,
      debugShowCheckedModeBanner: false,
      buildTransition: (_, __, child) => child,
    );
  }
}
