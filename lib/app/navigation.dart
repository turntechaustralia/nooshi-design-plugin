import 'package:flayout/home/main/page.dart';
import 'package:vrouter/vrouter.dart';

class FlayoutNavigation {
  static final instance = FlayoutNavigation._();

  FlayoutNavigation._();

  final routerDelegate = [
    VWidget(
      path: '/',
      widget: const EditorPage(),
    ),
  ];
}
