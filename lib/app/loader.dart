import 'package:flayout/app/app.dart' deferred as app;
import 'package:flutter/material.dart';

class AppLoader extends StatefulWidget {
  const AppLoader({Key? key}) : super(key: key);

  @override
  _AppLoaderState createState() => _AppLoaderState();
}

class _AppLoaderState extends State<AppLoader> {
  bool isInitialized = false;

  @override
  void initState() {
    app.loadLibrary().then((void _) async {
      await Future.delayed(const Duration(seconds: 1));
      setState(() => isInitialized = true);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!isInitialized) {
      return const Center(child: CircularProgressIndicator());
    } else {
      return app.EditorApp();
    }
  }
}
