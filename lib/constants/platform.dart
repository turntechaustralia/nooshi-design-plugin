import 'package:flutter/foundation.dart';

bool get isTargetMobile => [TargetPlatform.android, TargetPlatform.iOS]
    .contains(defaultTargetPlatform);

bool get isTargetDesktop => [
      TargetPlatform.linux,
      TargetPlatform.windows,
      TargetPlatform.macOS
    ].contains(defaultTargetPlatform);
