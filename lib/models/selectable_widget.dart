import 'dart:math' as math show pi;

import 'package:flutter/widgets.dart';

class SelectableWidget {
  /// widget center x position / canvas width
  double cx;

  /// widget center y position / canvas height
  double cy;

  /// widget rotation in degrees
  double r = 0;

  /// widget rotation in radians
  double get rd => r * -(math.pi / 180);

  /// widget corner radius in percent
  double cr = 0.00;

  /// is widget locked on canvas
  bool isLocked;

  /// is widget visible on canvas
  bool isVisible;

  /// widget's dynamic data
  final dynamic data;

  /// widget's layer link
  final link = LayerLink();

  /// widget's unique key
  final key = UniqueKey();

  /// widget's context key
  final contextKey = GlobalKey();

  SelectableWidget({
    this.cx = 0,
    this.cy = 0,
    required this.data,
  })  : isLocked = false,
        isVisible = true;

  @protected
  @mustCallSuper
  void copyWith(final SelectableWidgetSettings settings) {
    cr = settings.cr ?? cr;
  }

  Map<String, dynamic> toMap() {
    return {
      'cx': cx,
      'cy': cy,
      'data': data,
    };
  }

  @override
  String toString() {
    return toMap().toString();
  }
}

class SelectableWidgetSettings {
  final double? cr;

  const SelectableWidgetSettings({
    this.cr,
  });
}
