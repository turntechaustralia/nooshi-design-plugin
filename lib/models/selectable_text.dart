import 'dart:ui';

import 'package:flayout/models/selectable_widget.dart';
import 'package:flutter/cupertino.dart';

class SelectableText extends SelectableWidget {
  final textFocusNode = FocusNode();
  TextAlign textAlign = TextAlign.left;
  TextStyle textStyle = const TextStyle(fontFamily: 'Arial');

  SelectableText({
    double cx = 0.5,
    double cy = 0.5,
    required TextEditingController data,
  }) : super(
          cx: cx,
          cy: cy,
          data: data,
        );

  @override
  TextEditingController get data => super.data as TextEditingController;

  bool get isBolded => textStyle.fontWeight == FontWeight.bold;
  bool get isItalic => textStyle.fontStyle == FontStyle.italic;
  bool get isUnderlined =>
      textStyle.decoration?.contains(TextDecoration.underline) ?? false;

  bool get isLeftAligned => textAlign == TextAlign.left;
  bool get isRightAligned => textAlign == TextAlign.right;
  bool get isCenterAligned => textAlign == TextAlign.center;

  String colorRGBA() {
    final color = textStyle.color!;
    final red = color.red;
    final green = color.green;
    final blue = color.blue;
    final alpha = color.opacity;
    return 'rgba($red, $green, $blue, $alpha)';
  }

  @override
  void copyWith(covariant final SelectableTextSettings settings) {
    TextDecoration? newDecoration() {
      if (settings.isUnderlined == null) return null;
      return settings.isUnderlined!
          ? TextDecoration.underline
          : TextDecoration.none;
    }

    final newStyle = textStyle.copyWith(
      color: settings.color,
      fontSize: settings.fontSize,
      fontStyle: settings.fontStyle,
      fontWeight: settings.fontWeight,
      decoration: newDecoration(),
    );
    textStyle = newStyle;
    textAlign = settings.textAlign ?? textAlign;
    super.copyWith(settings);
  }

  Size layout({final double canvasHeight = 1080}) {
    final scale = canvasHeight / 1080;
    final painter = TextPainter(
      textAlign: textAlign,
      textDirection: TextDirection.ltr,
      text: TextSpan(
        text: data.text,
        style: textStyle.copyWith(fontSize: textStyle.fontSize! * scale),
      ),
    );
    painter.layout();
    return Size(painter.width, painter.height);
  }

  Text text({final double canvasHeight = 1080}) {
    final scale = canvasHeight / 1080;
    if (data.text.isEmpty) {
      return Text(
        ' ',
        textScaleFactor: 1,
        textAlign: textAlign,
        style: TextStyle(fontSize: textStyle.fontSize! * scale),
      );
    }
    return Text(
      data.text,
      textScaleFactor: 1,
      textAlign: textAlign,
      style: textStyle.copyWith(fontSize: textStyle.fontSize! * scale),
    );
  }
}

class SelectableTextSettings extends SelectableWidgetSettings {
  final Color? color;
  final double? fontSize;
  final bool? isUnderlined;
  final FontStyle? fontStyle;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;

  const SelectableTextSettings({
    this.color,
    this.fontSize,
    this.fontStyle,
    this.textAlign,
    this.fontWeight,
    this.isUnderlined,
    final double? cr,
  }) : super(cr: cr);
}
