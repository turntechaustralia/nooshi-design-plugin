import 'dart:math' show min;
import 'dart:typed_data';

import 'package:flayout/models/selectable_widget.dart';
import 'package:flutter/widgets.dart';

class SelectableImage extends SelectableWidget {
  final String extension;
  double scaleY;
  double scaleX;
  BoxFit _fit = BoxFit.cover;
  Alignment _alignment = Alignment.center;

  SelectableImage({
    double cx = 0,
    double cy = 0,
    this.scaleX = 0,
    this.scaleY = 0,
    required this.extension,
    required final Uint8List data,
  }) : super(
          cx: cx,
          cy: cy,
          data: data,
        );

  @override
  Uint8List get data => super.data as Uint8List;

  BoxFit get fit => _fit;

  Alignment get alignment => _alignment;

  @override
  void copyWith(covariant final SelectableImageSettings settings) {
    _fit = settings.fit ?? _fit;
    _alignment = settings.alignment ?? _alignment;
    super.copyWith(settings);
  }

  Widget image({
    final double? canvasWidth,
    final double? canvasHeight,
    final bool applySettings = true,
  }) {
    final width = canvasWidth == null ? null : canvasWidth * scaleX;
    final height = canvasHeight == null ? null : canvasHeight * scaleY;
    final image = Image.memory(
      data,
      width: width,
      height: height,
      fit: applySettings ? _fit : BoxFit.contain,
      alignment: applySettings ? _alignment : Alignment.center,
    );
    final double cr;
    if (width != null && height != null) {
      cr = this.cr / 100 * min(width, height);
    } else {
      cr = 0;
    }
    return ClipPath(
      clipBehavior: Clip.hardEdge,
      clipper: _CustomClipper(radius: cr),
      child: image,
    );
  }
}

class SelectableImageSettings extends SelectableWidgetSettings {
  final BoxFit? fit;
  final Alignment? alignment;

  const SelectableImageSettings({
    this.fit,
    this.alignment,
    final double? cr,
  }) : super(cr: cr);
}

class _CustomClipper extends CustomClipper<Path> {
  final double radius;

  const _CustomClipper({
    required this.radius,
  });

  @override
  Path getClip(Size size) {
    final path = Path();
    final width = size.width;
    final height = size.height;
    path.moveTo(radius, 0);
    path.lineTo(width - radius, 0);
    path.quadraticBezierTo(
      width,
      0,
      width,
      radius,
    );
    path.lineTo(width, height - radius);
    path.quadraticBezierTo(
      width,
      height,
      width - radius,
      height,
    );
    path.lineTo(radius, height);
    path.quadraticBezierTo(
      0,
      height,
      0,
      height - radius,
    );
    path.lineTo(0, radius);
    path.quadraticBezierTo(0, 0, radius, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    if (oldClipper.runtimeType != _CustomClipper) return true;
    final typedOldClipper = oldClipper as _CustomClipper;
    return typedOldClipper.radius != radius;
  }
}
