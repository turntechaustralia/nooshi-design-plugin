import 'package:flayout/providers/canvas/abstract.dart';
import 'package:flayout/providers/canvas/color_provider.dart';
import 'package:flayout/providers/canvas/context_menu_provider.dart';
import 'package:flayout/providers/canvas/radius_provider.dart';
import 'package:flayout/providers/canvas/selectable_widgets_provider.dart';
import 'package:flayout/providers/canvas/size_provider.dart';
import 'package:flutter/widgets.dart';

class CanvasProvider extends ChangeNotifier {
  late final ColorProvider colorProvider;
  late final ContextMenuProvider  contextMenuProvider;
  late final RadiusProvider radiusProvider;
  late final SelectableWidgetsProvider selectableWidgetsProvider;
  late final SizeProvider sizeProvider;

  CanvasProvider({
    final Color color = const Color(0xFFE0E0E0),
    final double aspectRatio = 16 / 9,
    final double radius = 0,
  }) {
    colorProvider = ColorProvider(this, color);
    contextMenuProvider = ContextMenuProvider(this);
    radiusProvider = RadiusProvider(this, radius);
    selectableWidgetsProvider = SelectableWidgetsProvider(this);
    sizeProvider = SizeProvider(this, aspectRatio);
  }

  void listenTo(final CanvasAbstractProvider provider) {
    provider.addListener(notifyListeners);
  }
}
