import 'dart:async';
import 'dart:convert' as convert;
import 'dart:html';
import 'dart:math' show min, max;

import 'package:flayout/components/preview.dart';
import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart' as st;
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class KeyProvider extends ChangeNotifier {
  final GlobalKey canvasKey = GlobalKey();
  final GlobalKey canvasParentKey = GlobalKey();

  bool _isProccessing = false;
  bool get isProccessing => _isProccessing;

  Future<Offset> localSelectablePositionToGlobal(
    final SelectableWidget selectedWidget,
  ) async {
    await Future.delayed(Duration.zero);
    final offset = selectedWidget.link.debugLeader?.offset;
    final canvasRenderBox =
        canvasKey.currentContext?.findRenderObject() as RenderBox?;
    final canvasParentRenderBox =
        canvasParentKey.currentContext?.findRenderObject() as RenderBox?;
    if (canvasParentRenderBox == null ||
        canvasRenderBox == null ||
        offset == null) return Offset.zero;
    final globalOffset = canvasRenderBox.localToGlobal(offset);
    final localOffset = canvasParentRenderBox.globalToLocal(globalOffset);
    return localOffset;
  }

  Future<void> previewImage(final BuildContext ctx) async {
    final canvasProvider = ctx.read<CanvasProvider>();
    final List<SelectableWidget> selectableWidgets =
        canvasProvider.selectableWidgetsProvider.selectableWidgets;
    _isProccessing = true;
    notifyListeners();
    await Future.delayed(const Duration(milliseconds: 120));
    const width = 1920;
    const height = 1080;
    final canvas = CanvasElement(
      width: width,
      height: height,
    );
    final context = canvas.context2D;
    final color = canvasProvider.colorProvider.color;
    context.globalCompositeOperation = 'destination-behind';
    context.fillStyle =
        'rgba(${color.red}, ${color.green}, ${color.blue}, ${color.opacity})';
    context.fillRect(0, 0, width, height);
    for (final widget in selectableWidgets) {
      final cx = widget.cx * width;
      final cy = widget.cy * height;
      final rd = widget.rd;
      context.save();
      context.translate(cx, cy);
      context.rotate(rd);
      if (widget is SelectableImage) {
        final w = widget.scaleX * width;
        final h = widget.scaleY * height;
        final imageData = convert.base64.encode(widget.data);
        final image = ImageElement(
          src: 'data:image/${widget.extension};base64,$imageData',
        );
        final c = Completer<Event>();
        image.onLoad.listen(c.complete);
        await c.future;
        final imageWidth = image.width!;
        final imageHeight = image.height!;
        final Rectangle? sourceRect;
        if (widget.fit == BoxFit.fill) {
          sourceRect = null;
        } else {
          final destRatio = w / h;
          final imageRatio = imageWidth / imageHeight;
          if (destRatio < imageRatio) {
            final cropWidth = imageHeight * destRatio;
            final double left;
            if (widget.alignment == Alignment.centerLeft) {
              left = 0;
            } else if (widget.alignment == Alignment.centerRight) {
              left = imageWidth - cropWidth;
            } else {
              left = (imageWidth - cropWidth) / 2;
            }
            sourceRect = Rectangle(left, 0, cropWidth, imageHeight);
          } else {
            final cropHeight = imageWidth / destRatio;
            final double top;
            if (widget.alignment == Alignment.topCenter) {
              top = 0;
            } else if (widget.alignment == Alignment.bottomCenter) {
              top = imageHeight - cropHeight;
            } else {
              top = (imageHeight - cropHeight) / 2;
            }
            sourceRect = Rectangle(0, top, imageWidth, cropHeight);
          }
        }
        //clipping proccess
        final radius = widget.cr / 100 * min(w, h);
        context.beginPath();
        context.moveTo(-w / 2 + radius, -h / 2);
        context.lineTo(-w / 2 + w - radius, -h / 2);
        context.quadraticCurveTo(
          -w / 2 + w,
          -h / 2,
          -w / 2 + w,
          -h / 2 + radius,
        );
        context.lineTo(-w / 2 + w, -h / 2 + h - radius);
        context.quadraticCurveTo(
          -w / 2 + w,
          -h / 2 + h,
          -w / 2 + w - radius,
          -h / 2 + h,
        );
        context.lineTo(-w / 2 + radius, -h / 2 + h);
        context.quadraticCurveTo(
          -w / 2,
          -h / 2 + h,
          -w / 2,
          -h / 2 + h - radius,
        );
        context.lineTo(-w / 2, -h / 2 + radius);
        context.quadraticCurveTo(-w / 2, -h / 2, -w / 2 + radius, -h / 2);
        context.closePath();
        context.clip();
        //drawing image
        context.drawImageToRect(
          image,
          Rectangle(-w / 2, -h / 2, w, h),
          sourceRect: sourceRect,
        );
        image.remove();
      } else if (widget is st.SelectableText) {
        final fontSize = widget.textStyle.fontSize!;
        final fontFamily = widget.textStyle.fontFamily!;
        final color = widget.colorRGBA();
        final size = widget.layout();
        final h = size.height;
        num w = 0;
        context.font =
            "${widget.isBolded ? 'bold' : 'normal'} ${fontSize}px $fontFamily";
        for (final s in widget.data.text.split('\n')) {
          final _w = context.measureText(s).width ?? 0;
          w = max(_w, w);
        }
        final data = '''
              data:image/svg+xml,
              <svg xmlns='http://www.w3.org/2000/svg' width='$w' height='$h'>
                <foreignObject width='100%' height='100%'>
                  <div xmlns='http://www.w3.org/1999/xhtml'
                    style=' font-size:${fontSize}px;
                            font-family:$fontFamily;
                            color:$color;
                            text-align:${describeEnum(widget.textAlign)};
                            font-weight:${widget.isBolded ? 700 : 400};
                            font-style:${widget.isItalic ? 'italic' : 'normal'};
                            text-decoration:${widget.isUnderlined ? 'underline' : 'none'}; '>
                    ${widget.data.text}
                  </div>
                </foreignObject>
              </svg>''';
        final image = ImageElement(src: data);
        final c = Completer();
        image.onLoad.listen((_) {
          c.complete();
        });
        await c.future;
        context.drawImageToRect(image, Rectangle(-w / 2, -h / 2, w, h));
        image.remove();
      }
      context.restore();
    }
    final base64 = canvas.toDataUrl();
    canvas.remove();
    // final anchorElement = AnchorElement(href: base64);
    // anchorElement.setAttribute(
    //   'download',
    //   'image-${DateTime.now().millisecondsSinceEpoch}.png',
    // );
    // anchorElement.click();
    // anchorElement.remove();
    _isProccessing = false;
    notifyListeners();
    CanvasPreview.showPreview(context: ctx, base64data: base64);
  }
}
