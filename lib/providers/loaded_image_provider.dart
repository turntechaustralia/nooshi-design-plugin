import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class LoadedImageProvider extends ChangeNotifier {
  final Map<String, double?> _urlImages = {};
  final List<Uint8List> _images = [];
  final List<String> _imageExts = [];

  int get loadedImageCount => _images.length;
  int get loadingImageCount => _urlImages.length;
  int get imageCount => loadedImageCount + loadingImageCount;
  Uint8List loadedImageAt(final int index) => _images[index];
  String loadedImageExtAt(final int index) => _imageExts[index];
  List<String> get loadingImageKeys => _urlImages.keys.toList();
  double? loadingImageProgressFor(final String url) => _urlImages[url];

  void removeLoadedImageAt(final int index) {
    _images.removeAt(index);
    _imageExts.removeAt(index);
    notifyListeners();
  }

  Future<void> addImage() async {
    final result = await FilePicker.platform.pickFiles(
      withData: true,
      allowMultiple: true,
      type: FileType.custom,
      allowCompression: false,
      allowedExtensions: [
        'jpg',
        'jpeg',
        'png',
        'bmp',
      ],
    );
    if (result == null) return;
    for (final file in result.files) {
      final bytes = file.bytes;
      if (bytes != null) {
        _images.add(bytes);
        _imageExts.add(file.extension!);
      }
      notifyListeners();
    }
  }

  Future<void> addImageByUrl(final String url) async {
    const url =
        'https://images.pexels.com/photos/1382734/pexels-photo-1382734.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';
    final completer = Completer<void>();
    _urlImages[url] = null;
    const NetworkImage(url).resolve(ImageConfiguration.empty).addListener(
          ImageStreamListener(
            (imageUnfo, __) async {
              final image = (await imageUnfo.image
                      .toByteData(format: ImageByteFormat.png))!
                  .buffer
                  .asUint8List();
              _images.add(image);
              _imageExts.add('png');
              completer.complete();
            },
            onChunk: (chunk) {
              if (!_urlImages.containsKey(url)) return;
              final expected = chunk.expectedTotalBytes;
              final double? progress;
              if (expected == null) {
                progress = null;
              } else {
                progress =
                    chunk.cumulativeBytesLoaded / chunk.expectedTotalBytes!;
              }
              _urlImages[url] = progress;
              notifyListeners();
            },
          ),
        );
    _urlImages.remove(url);
    await completer.future;
    notifyListeners();
  }
}
