import 'package:flayout/models/sections.dart';
import 'package:flutter/material.dart';

class ToolboxProvider extends ChangeNotifier {
  Section? _chosenSection;
  bool _isToolboxOpen = false;
  late Section _lastChosenSection = _chosenSection ?? Section.image;
  final animationDuration = const Duration(milliseconds: 350);
  final scrollController = ScrollController();
  final reorderScrollController = ScrollController();

  bool get isToolboxOpen => _isToolboxOpen;
  Section? get chosenSection => _chosenSection;
  Section get lastChosenSection => _lastChosenSection;
  bool get isSectionOpen => _chosenSection != null;
  bool get isSectionText => _chosenSection == Section.text;
  bool get isSectionImage => _chosenSection == Section.image;
  bool get isSectionCanvas => _chosenSection == Section.canvas;

  void switchToolboxState() {
    _isToolboxOpen = !_isToolboxOpen;
    notifyListeners();
  }

  void onSectionTapped([final Section? section]) {
    _lastChosenSection = section ?? _lastChosenSection;
    if (section == null || _chosenSection == section) {
      _chosenSection = null;
    } else {
      _chosenSection = section;
    }
    notifyListeners();
  }

  Future<void> resetScrollController() async {
    if (!scrollController.hasClients) return;
    return scrollController.animateTo(
      0,
      curve: Curves.easeInOut,
      duration: const Duration(milliseconds: 350),
    );
  }
}
