import 'package:flayout/providers/canvas/abstract.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/material.dart';

class ContextMenuProvider extends CanvasAbstractProvider {
  bool _contextMenuVisibility = false;
  Offset _contextMenuLocation = Offset.zero;

  ContextMenuProvider(CanvasProvider canvasProvider) : super(canvasProvider);

  Offset get contextMenuLocation => _contextMenuLocation;
  bool get contextMenuVisibility => _contextMenuVisibility;

  void hideContextMenu() {
    _contextMenuVisibility = false;
    notifyListeners();
  }

  void showContextMenu({
    required final Offset location,
  }) {
    _contextMenuLocation = location;
    _contextMenuVisibility = true;
    notifyListeners();
  }
}
