import 'package:flayout/providers/canvas/abstract.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/painting.dart';

class ColorProvider extends CanvasAbstractProvider {
  Color _color;

  ColorProvider(
    CanvasProvider canvasProvider,
    this._color,
  ) : super(canvasProvider);

  Color get color => _color;

  void setColor(final Color newColor) {
    _color = newColor;
    notifyListeners();
  }
}
