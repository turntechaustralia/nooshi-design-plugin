import 'dart:math' show cos, sin, pi, atan2;

import 'package:flayout/models/corner.dart';
import 'package:flayout/models/middle.dart';
import 'package:flayout/models/selectable_image.dart';
import 'package:flayout/models/selectable_text.dart';
import 'package:flayout/models/selectable_widget.dart';
import 'package:flayout/providers/canvas/abstract.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/widgets.dart';

class SelectableWidgetsProvider extends CanvasAbstractProvider {
  SelectableWidget? _selectedWidget;
  final List<SelectableWidget> _widgets = [];

  SelectableWidgetsProvider(CanvasProvider canvasProvider)
      : super(canvasProvider);

  int get selectableWidgetsCount => _widgets.length;
  SelectableWidget? get selectedWidget => _selectedWidget;
  SelectableWidget widgetAt(final int index) => _widgets[index];
  int? get selectedWidgetIndex =>
      _selectedWidget == null ? null : _widgets.indexOf(_selectedWidget!);
  List<SelectableWidget> get selectableWidgets => _widgets;

  void addSelectableWidget(final SelectableWidget selectableWidget) {
    if (selectableWidget is SelectableText) {
      selectableWidget.data.addListener(notifyListeners);
    }
    _widgets.add(selectableWidget);
    _selectedWidget = selectableWidget;
    notifyListeners();
  }

  void removeSelectedWidget([final int? index]) {
    final idx = index ?? selectedWidgetIndex;
    if (idx == null) return;
    canvasProvider.contextMenuProvider.hideContextMenu();
    _selectedWidget!.isVisible = false;
    _selectedWidget = null;
    notifyListeners();
    Future.delayed(
      const Duration(milliseconds: 350),
      () {
        _widgets.removeAt(idx);
        notifyListeners();
      },
    );
  }

  void selectableWidgetOnTapAt([final int? index]) {
    final widget = _selectedWidget;
    if (widget is SelectableText && index != selectedWidgetIndex) {
      widget.textFocusNode.unfocus();
    }
    if (index == null) {
      if (!canvasProvider.contextMenuProvider.contextMenuVisibility) {
        _selectedWidget = null;
      }
    } else {
      _selectedWidget = widgetAt(index);
    }
    canvasProvider.contextMenuProvider.hideContextMenu();
    notifyListeners();
  }

  void selectableWidgetOnSecendaryTapUpAt([
    final int? index,
    final TapUpDetails? details,
    final GlobalKey? canvasParentKey,
  ]) {
    if (canvasProvider.contextMenuProvider.contextMenuVisibility ||
        details == null ||
        canvasParentKey == null) {
      canvasProvider.contextMenuProvider.hideContextMenu();
      return notifyListeners();
    }
    if (index != null) selectableWidgetOnTapAt(index);
    final canvasParentRenderBox =
        canvasParentKey.currentContext?.findRenderObject() as RenderBox?;
    if (canvasParentRenderBox == null) return;
    final localPosition =
        canvasParentRenderBox.globalToLocal(details.globalPosition);
    canvasProvider.contextMenuProvider.showContextMenu(location: localPosition);
    notifyListeners();
  }

  void selectableWidgetOnLongPressStartAt([
    final int? index,
    final LongPressStartDetails? details,
    final GlobalKey? canvasParentKey,
  ]) {
    final _details = details == null
        ? null
        : TapUpDetails(
            kind: PointerDeviceKind.unknown,
            localPosition: details.localPosition,
            globalPosition: details.globalPosition,
          );
    selectableWidgetOnSecendaryTapUpAt(index, _details, canvasParentKey);
  }

  void onPanUpdate(final DragUpdateDetails details) {
    final widget = selectedWidget;
    if (widget == null) return;
    canvasProvider.contextMenuProvider.hideContextMenu();
    final dx =
        details.delta.dx * cos(widget.rd) - details.delta.dy * sin(widget.rd);
    final dy =
        details.delta.dx * sin(widget.rd) + details.delta.dy * cos(widget.rd);
    widget.cx += dx / canvasProvider.sizeProvider.width;
    widget.cy += dy / canvasProvider.sizeProvider.height;
    notifyListeners();
  }

  void onSelectableWidgetReordered(final int oldOrder, final int newOrder) {
    final selectableWidget = _widgets.removeAt(oldOrder);
    _widgets.insert(newOrder, selectableWidget);
    notifyListeners();
  }

  void onSelectableWidgetMoveUp() {
    final index = selectedWidgetIndex;
    if (index == null || (index + 1) == selectableWidgetsCount) return;
    final widget = _widgets.removeAt(index);
    _widgets.insert(index + 1, widget);
    notifyListeners();
  }

  void onSelectableWidgetMoveDown() {
    final index = selectedWidgetIndex;
    if (index == null || index == 0) return;
    final widget = _widgets.removeAt(index);
    _widgets.insert(index - 1, widget);
    notifyListeners();
  }

  void changeIsWidgetLockedAt([final int? index]) {
    final selectableWidget = index == null ? _selectedWidget : widgetAt(index);
    if (selectableWidget == null) return;
    selectableWidget.isLocked = !selectableWidget.isLocked;
    notifyListeners();
  }

  void changeIsWidgetVisibleAt([final int? index]) {
    final selectableWidget = index == null ? _selectedWidget : widgetAt(index);
    if (selectableWidget == null) return;
    selectableWidget.isVisible = !selectableWidget.isVisible;
    notifyListeners();
  }

  void onImageResize({
    required final DragUpdateDetails details,
    required final Middle middle,
  }) {
    canvasProvider.contextMenuProvider.hideContextMenu();
    final selectedWidget = _selectedWidget;
    if (selectedWidget == null || selectedWidget is! SelectableImage) return;
    final selectedImage = selectedWidget;
    final diff = details.primaryDelta!;
    if (diff < 0) {
      if ([Middle.left, Middle.right].contains(middle) &&
          selectedImage.scaleX < .05) {
        return;
      } else if ([Middle.top, Middle.bottom].contains(middle) &&
          selectedImage.scaleY < .05) {
        return;
      }
    }

    double adjustVerticalDrag() {
      final diffY = diff / canvasProvider.sizeProvider.height;
      final diffX = diff / canvasProvider.sizeProvider.width;
      selectedImage.cy += diffY / 2 * cos(selectedImage.rd);
      selectedImage.cx -= diffX / 2 * sin(selectedImage.rd);
      return diffY;
    }

    double adjustHorizontalDrag() {
      final diffY = diff / canvasProvider.sizeProvider.height;
      final diffX = diff / canvasProvider.sizeProvider.width;
      selectedImage.cy += diffY / 2 * sin(selectedImage.rd);
      selectedImage.cx += diffX / 2 * cos(selectedImage.rd);
      return diffX;
    }

    switch (middle) {
      case Middle.top:
        selectedImage.scaleY -= adjustVerticalDrag();
        break;
      case Middle.bottom:
        selectedImage.scaleY += adjustVerticalDrag();
        break;
      case Middle.right:
        selectedImage.scaleX += adjustHorizontalDrag();
        break;
      case Middle.left:
        selectedImage.scaleX -= adjustHorizontalDrag();
        break;
    }
    notifyListeners();
  }

  void onImageRescale({
    required final DragUpdateDetails details,
    required final Corner corner,
  }) {
    canvasProvider.contextMenuProvider.hideContextMenu();
    final selectedWidget = _selectedWidget;
    if (selectedWidget == null || selectedWidget is! SelectableImage) return;
    final selectedImage = selectedWidget;
    final isLeft = [Corner.bottomLeft, Corner.topLeft].contains(corner);
    final diff = details.primaryDelta! /
        canvasProvider.sizeProvider.width *
        (isLeft ? 1 : -1);
    final diffY = diff / selectedImage.scaleX * selectedImage.scaleY;
    if (diff > 0) {
      if (selectedImage.scaleY < .05 || selectedImage.scaleX < .05) {
        return;
      }
    }
    switch (corner) {
      case Corner.bottomRight:
        // selectedImage.cx -= diff / 2;
        // selectedImage.cy -= diffY / 2;
        break;
      case Corner.bottomLeft:
        // selectedImage.cx += diff / 2;
        // selectedImage.cy -= diffY / 2;
        break;
      case Corner.topRight:
        // selectedImage.cx -= diff / 2;
        // selectedImage.cy += diffY / 2;
        break;
      case Corner.topLeft:
        // selectedImage.cx += diff / 2;
        // selectedImage.cy += diffY / 2;
        break;
    }
    selectedImage.scaleX -= diff;
    selectedImage.scaleY -= diffY;
    notifyListeners();
  }

  void setImageNewSettings({
    required final SelectableImage image,
    required final SelectableImageSettings settings,
  }) {
    image.copyWith(settings);
    notifyListeners();
  }

  void setTextNewSettings({
    required final SelectableText text,
    required final SelectableTextSettings settings,
  }) {
    text.copyWith(settings);
    notifyListeners();
  }

  void onWidgetRotation([final double degrees = 90]) {
    final widget = selectedWidget;
    if (widget == null) return;
    widget.r = (widget.r + degrees).remainder(360);
    notifyListeners();
  }

  void onWidgetRotationDrag(final DragUpdateDetails details) {
    final widget = selectedWidget;
    final size = widget?.link.leaderSize;
    final context = widget?.contextKey.currentContext;
    final renderBox = context?.findRenderObject() as RenderBox?;
    if (widget == null || size == null || renderBox == null) return;

    final localPosition = renderBox.globalToLocal(details.globalPosition);
    final localPositionByCenter = size.center(-localPosition);
    final dx = -localPositionByCenter.dy;
    final dy = localPositionByCenter.dx;
    if (dx == 0) {
      if (dy > 0) {
        widget.r = 90;
      } else {
        widget.r = 270;
      }
    } else {
      final rad = atan2(dx, dy);
      final degrees = rad * (180 / pi) + 90 + 360;
      widget.r = degrees.remainder(360);
    }
    notifyListeners();
  }
}
