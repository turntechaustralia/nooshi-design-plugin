import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/foundation.dart';

class CanvasAbstractProvider extends ChangeNotifier {
  final CanvasProvider canvasProvider;

  CanvasAbstractProvider(this.canvasProvider) {
    canvasProvider.listenTo(this);
  }
}
