import 'dart:math' show max;

import 'package:flayout/constants/platform.dart';
import 'package:flayout/providers/canvas/abstract.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/widgets.dart';

class SizeProvider extends CanvasAbstractProvider {
  late final controller = TransformationController()
    ..addListener(canvasProvider.contextMenuProvider.hideContextMenu);
  Size _latestCanvasAreaSizeReceived = Size.zero;
  bool _isBigLayout = false;
  final double aspectRatio;
  double _scale = 1;
  double? _height;
  double? _width;

  SizeProvider(
    CanvasProvider canvasProvider,
    this.aspectRatio,
  ) : super(canvasProvider);

  double get scale => _scale;
  double get width => (_width ?? 0) * _scale;
  double get height => (_height ?? 0) * _scale;

  bool get isBigLayout => _isBigLayout;
  void setIsBigLayout({required final bool isBigLayout}) {
    if (_isBigLayout == isBigLayout) return;
    _isBigLayout = isBigLayout;
    Future(notifyListeners);
  }

  void setInitialSize(final Size size) {
    if (_latestCanvasAreaSizeReceived != size) {
      Future(canvasProvider.contextMenuProvider.hideContextMenu);
    }
    _latestCanvasAreaSizeReceived = size;
    if (_width != null) return;
    if (aspectRatio * size.height > size.width) {
      _width = size.width * .9;
      _height = _width! / aspectRatio;
    } else {
      _height = size.height * .9;
      _width = _height! * aspectRatio;
    }
    final double dx = (size.width - _width!) / 2;
    final double dy = (size.height - _height!) / 2;
    translateView(dx, dy);
  }

  void resetCanvasSize() {
    _width = _height = null;
    setInitialSize(_latestCanvasAreaSizeReceived);
  }

  void zoomIn([final double? value]) {
    final w1 = width;
    final h1 = height;
    if (value == null) {
      _scale /= .9;
    } else {
      _scale -= value / 10000;
    }
    final w2 = width;
    final h2 = height;
    final wd = w1 - w2;
    final hd = h1 - h2;
    translateView(wd / 2, hd / 2);
    notifyListeners();
  }

  void zoomOut([final double? value]) {
    final w1 = width;
    final h1 = height;
    if (value == null) {
      _scale *= .9;
    } else {
      _scale -= value / 10000;
    }
    _scale = max(_scale, .01);
    final w2 = width;
    final h2 = height;
    final wd = w1 - w2;
    final hd = h1 - h2;
    translateView(wd / 2, hd / 2);
    notifyListeners();
  }

  double _initialScaleValue = 1;
  Offset _initialScaleFocalPoint = Offset.zero;

  void onScaleStart(final ScaleStartDetails details) {
    if (isTargetDesktop) return;
    _initialScaleFocalPoint = details.focalPoint;
    _initialScaleValue = 1;
  }

  void onScaleUpdate(final ScaleUpdateDetails details) {
    if (isTargetDesktop) return;
    if (details.pointerCount == 1) return;
    _initialScaleValue = details.scale;
    final offset = details.focalPoint - _initialScaleFocalPoint;
    _initialScaleFocalPoint = details.focalPoint;
    translateView(offset.dx, offset.dy);
    double scaleValue = details.scale / _initialScaleValue;
    if (scaleValue < 1) scaleValue = 1 / scaleValue;
    scaleValue *= 50;
    if (details.scale > _initialScaleValue) {
      zoomIn(-scaleValue);
    } else {
      zoomOut(scaleValue);
    }
  }

  void translateView(final double x, final double y) {
    controller.value = controller.value..translate(x, y);
    Future(notifyListeners);
  }
}
