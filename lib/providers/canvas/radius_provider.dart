import 'package:flayout/providers/canvas/abstract.dart';
import 'package:flayout/providers/canvas_provider.dart';
import 'package:flutter/painting.dart';

class RadiusProvider extends CanvasAbstractProvider {
  double _radius;

  RadiusProvider(
    CanvasProvider canvasProvider,
    this._radius,
  ) : super(canvasProvider);

  double get rawRadius => _radius;
  BorderRadius get radius => BorderRadius.all(Radius.circular(_radius));

  void setBorderRadius(final double newRadius) {
    if (newRadius < 0) {
      _radius = 0;
    } else {
      _radius = newRadius;
    }
    notifyListeners();
  }
}
