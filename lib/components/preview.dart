import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';

class CanvasPreview extends StatefulWidget {
  final OverlayEntry entry;
  final Uint8List image;

  const CanvasPreview._({
    Key? key,
    required this.entry,
    required this.image,
  }) : super(key: key);

  static void showPreview({
    required final BuildContext context,
    required final String base64data,
  }) {
    final image = const Base64Decoder().convert(base64data, 22);
    final overlayState = Overlay.of(context);
    if (overlayState == null) return;
    late final OverlayEntry entry;
    entry = OverlayEntry(
      builder: (ctx) => CanvasPreview._(
        entry: entry,
        image: image,
      ),
    );
    overlayState.insert(entry);
  }

  @override
  _CanvasPreviewState createState() => _CanvasPreviewState();
}

class _CanvasPreviewState extends State<CanvasPreview> {
  double scale = 0;
  bool isAnimating = false;

  @override
  void initState() {
    super.initState();

    Future(() {
      setState(() {
        scale = 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: IgnorePointer(
        ignoring: isAnimating,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            setState(() {
              scale = 0;
              isAnimating = true;
            });
          },
          child: IgnorePointer(
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 600),
              color: isAnimating
                  ? Colors.transparent
                  : Colors.grey.withOpacity(.5),
              padding: const EdgeInsets.all(16.0),
              child: SizedBox(
                width: double.infinity,
                height: double.infinity,
                child: Center(
                  child: AnimatedScale(
                    scale: scale,
                    curve: Curves.bounceOut,
                    duration: const Duration(milliseconds: 600),
                    onEnd: () {
                      if (scale == 0) widget.entry.remove();
                    },
                    child: Material(
                      color: Colors.transparent,
                      clipBehavior: Clip.hardEdge,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0),
                        side: const BorderSide(
                          width: 2,
                          color: Colors.blueAccent,
                        ),
                      ),
                      child: Image.memory(widget.image),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
