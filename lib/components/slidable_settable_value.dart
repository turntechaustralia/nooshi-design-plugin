import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SlidableSettableValue extends StatelessWidget {
  final String title;
  final double value;
  final double minValue;
  final double maxValue;
  final bool isLowValue;
  final ValueChanged<double> valueChanged;

  const SlidableSettableValue({
    Key? key,
    required this.title,
    required this.value,
    required this.minValue,
    required this.maxValue,
    this.isLowValue = false,
    required this.valueChanged,
  }) : super(key: key);

  void _onVerticalDragUpdate(final DragUpdateDetails details) {
    final _value = (value + details.delta.dy * -(isLowValue ? 1 : 30))
        .clamp(minValue, maxValue);
    valueChanged(_value);
  }

  void _onPointerSignal(final PointerSignalEvent event) {
    if (event is! PointerScrollEvent) return;
    final scrollDelta = event.scrollDelta * .1;
    final details = DragUpdateDetails(
      delta: scrollDelta,
      globalPosition: event.position,
      sourceTimeStamp: event.timeStamp,
      localPosition: event.localPosition,
    );
    _onVerticalDragUpdate(details);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title),
          const SizedBox(width: 12),
          Listener(
            onPointerSignal: _onPointerSignal,
            child: MouseRegion(
              cursor: SystemMouseCursors.resizeUpDown,
              child: GestureDetector(
                onVerticalDragUpdate: _onVerticalDragUpdate,
                child: Container(
                  height: 26,
                  alignment: Alignment.center,
                  constraints: const BoxConstraints(minWidth: 80),
                  margin: const EdgeInsetsDirectional.only(end: 24),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.grey),
                  ),
                  child: Text(
                    '${value ~/ .1 / 10}',
                    style: const TextStyle(height: 1),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
