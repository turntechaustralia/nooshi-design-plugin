import 'package:flayout/constants/platform.dart';
import 'package:flutter/material.dart';

class HoverOpacity extends StatefulWidget {
  final Curve curve;
  final Widget child;
  final double hoveredOpacity;
  final double unhoveredOpacity;

  const HoverOpacity({
    Key? key,
    required this.child,
    this.hoveredOpacity = 1,
    this.unhoveredOpacity = .3,
    this.curve = Curves.easeInOut,
  })  : assert(hoveredOpacity <= 1 && hoveredOpacity >= 0),
        assert(unhoveredOpacity <= 1 && unhoveredOpacity >= 0),
        super(key: key);

  @override
  _HoverOpacityState createState() => _HoverOpacityState();
}

class _HoverOpacityState extends State<HoverOpacity> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    if (isTargetMobile) return widget.child;
    return MouseRegion(
      onExit: (_) => setState(() => isHovered = false),
      onEnter: (_) => setState(() => isHovered = true),
      child: AnimatedOpacity(
        curve: widget.curve,
        duration: const Duration(milliseconds: 350),
        opacity: isHovered ? widget.hoveredOpacity : widget.unhoveredOpacity,
        child: widget.child,
      ),
    );
  }
}
