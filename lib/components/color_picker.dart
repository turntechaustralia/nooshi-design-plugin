import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart' as cp;

class ColorPicker extends StatefulWidget {
  final String title;
  final Color pickedColor;
  final ValueChanged<Color> pickedColorChanged;

  const ColorPicker({
    Key? key,
    required this.title,
    required this.pickedColor,
    required this.pickedColorChanged,
  }) : super(key: key);

  @override
  _ColorPickerState createState() => _ColorPickerState();
}

class _ColorPickerState extends State<ColorPicker> {
  late Color _pickerColor = widget.pickedColor;

  @override
  void didUpdateWidget(covariant ColorPicker oldWidget) {
    setState(() {
      _pickerColor = widget.pickedColor;
    });
    super.didUpdateWidget(oldWidget);
  }

  void setPickerColor(final Color newColor) =>
      setState(() => _pickerColor = newColor);

  Future<bool> onCancel() async {
    _pickerColor = widget.pickedColor;
    return true;
  }

  void pickColor() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return WillPopScope(
          onWillPop: onCancel,
          child: AlertDialog(
            title: const Text('Pick a color!'),
            content: SingleChildScrollView(
              child: cp.ColorPicker(
                pickerColor: _pickerColor,
                onColorChanged: setPickerColor,
                pickerAreaHeightPercent: 0.8,
              ),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  onCancel();
                  Navigator.of(context).pop();
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () {
                  widget.pickedColorChanged(_pickerColor);
                  Navigator.of(context).pop();
                },
                child: const Text('Got it'),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(widget.title),
          Padding(
            padding: const EdgeInsets.only(
              left: 12,
              right: 50,
            ),
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                onTap: pickColor,
                child: Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: widget.pickedColor,
                    border: Border.all(color: Colors.grey),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
